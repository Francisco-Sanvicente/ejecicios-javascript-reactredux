const express = require("express");
const Stripe = require("stripe");
require('dotenv').config();
const stripe = new Stripe(process.env.STRIPE_KEY_SECRET);

const cors = require("cors");

const app = express();

app.use(cors({ origin: process.env.URL_FRONTEND }));
app.use(express.json());

app.post("/api/checkout", async (req, res) => {
  // you can get more data to find in a database, and so on
  const {id, amount,  } = req.body;

  try {
    const payment = await stripe.paymentIntents.create({
      amount,
      currency: "USD",
      description: "Gaming Keyboard",
      payment_method: id,
      confirm: true, //confirm the payment at the same time
     
    });

    console.log(payment);

    return res.status(200).json({ message: "Pago Hecho Exitosamente" });
  } catch (error) {
    console.log(error);
    return res.json({ message: error.raw.message });
  }
});

app.listen(process.env.PORT, () => {
  console.log(`Server on port ${process.env.PORT}`);
});