import axios from 'axios';

const clienteAxios = axios.create({
    baseURL: process.env.URL_BACKEND,
    headers: {"Content-Type": "application/json"},
    
  });

export default clienteAxios;