
module.exports = {
  env: {
    URL_BACKEND: 'https://server-node-strapi.herokuapp.com',
    TOKEN: 'token',
    STRIPE_TOKEN: 'pk_test_51HQvbODBJ4O61d74Ff92s6WGY5SO5qIFdHUaxCgqjLaAOUbYbe4XLsj0mFJ2i1XqtKVyAxj2GvU0gLMgSQdP0WU200oabwWyjQ',
    URL_BACKEND_STRIPE: 'https://server-node-stripe.herokuapp.com/api/checkout'
  },
 
  exportPathMap: async function (
    defaultPathMap,
    { dev, dir, outDir, distDir, buildId }
  ) {
    return {
      "/": { page: "/" },
      "/login": { page: "/login" },
      "/crearcuenta": { page: "/crearcuenta" },
      "/search": { page: "/search" },
      "/paymentPage": { page: "/paymentPage" },
      "/legalPage": { page: "/legalPage" },
      "/detailPage": { page: "/detailPage" },
      "/detailPage/[slug]": { page: "/detailPage/[slug]" },
      "/deliveryPage": { page: "/deliveryPage" },
    }
  },
  target: "serverless",
  images: {
    domains: ["res.cloudinary.com"],
   
  },
  
};
