module.exports = {
  mode: 'jit',
  purge: ['./pages/**/*.{js,ts,jsx,tsx}', './components/**/*.{js,ts,jsx,tsx}'],
  darkMode: false, // or 'media' or 'class'
  theme: {
    extend: {
      maxHeight: {
        '9': '620px',
      },
      colors: {
        ligthblack: 'rgba(0, 0, 0, 0.3)'
      },
    },
  },
  variants: {
    extend: {},
  },
  plugins: [],
}
