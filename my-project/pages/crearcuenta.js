import { useState, useEffect, useContext } from "react";
import { useRouter } from "next/router";
import { useFormik } from "formik";
import * as Yup from "yup";
import authContext from "../components/context/auth/authContext";
import GlobalStyle from "../style/globalStyles";
import Layout from "../components/Layout";
import Spining from "../components/Spining";
import Navbar from "../components/navbar";
import Alerta from "../components/Alerta";

const Crearcuenta = () => {
  const [spining, setSpining] = useState(true);
  const router = useRouter();
  const AuthContext = useContext(authContext);

  const { mensaje, registrarUsuario, autenticado } = AuthContext;

  useEffect(() => {
    if(autenticado) {
      router.replace('/')
    }
      //eslint-disable-next-line
  }, [autenticado])

  const formik = useFormik({
    initialValues: {
      name: "",
      lastname: "",
      username: "",
      email: "",
      password: "",
    },
    validationSchema: Yup.object({
      name: Yup.string().required("El Nombre es Obligatorio"),
      lastname: Yup.string().required("El Apellido es Obligatorio"),
      username: Yup.string().required("El Nombre de usuaro es Obligatorio"),
      email: Yup.string()
        .email("El email no es válido")
        .required("El Email es Obligatorio"),
      password: Yup.string()
        .required("El password no puede ir vacio")
        .min(6, "El password debe contener al menos 6 caracteres"),
    }),
    onSubmit:  (valores) => {
      registrarUsuario(valores)
    },
  });

  useEffect(() => {
    setSpining(true);
    setTimeout(() => {
      setSpining(false);
    }, 1000);
  }, []);

  return (
    <>
      <Layout title="Sing Up">
        <GlobalStyle />
        <Spining spining={spining} />
        <Navbar />

        <div className="md:w-4/5 xl:w-3/5 mx-auto mb-32">
          <h2 className="text-4xl font-sans font-bold text-gray-800 text-center my-4">
            Crear Cuenta
          </h2>
          {mensaje && <Alerta />}

          <div className="flex justify-center mt-5">
            <div className="w-full max-w-lg">
              <form
                className="bg-white rounded shadow-md px-8 pt-6 pb-8 mb-4"
                onSubmit={formik.handleSubmit}
              >
                <div className="mb-4">
                  <label
                    className="block text-black text-sm font-bold mb-2"
                    htmlFor="nombre"
                  >
                    Nombre
                  </label>
                  <input
                    type="text"
                    className="shadow appearance-none rounded w-full py-2 px-3 text-gray-700 leading-tight border focus:outline-none focus:ring-2 focus:ring-purple-600 focus:border-transparent"
                    id="name"
                    name="name"
                    placeholder="Nombre de Usuario"
                    value={formik.values.name}
                    onChange={formik.handleChange}
                    onBlur={formik.handleBlur}
                  />

                  {formik.touched.name && formik.errors.name ? (
                    <div className="my-2 bg-gray-200 border-l-4 border-red-500 text-red-700 p-4">
                      <p className="font-bold">Error</p>
                      <p>{formik.errors.name} </p>
                    </div>
                  ) : null}
                </div>
                <div className="mb-4">
                  <label
                    className="block text-black text-sm font-bold mb-2"
                    htmlFor="nombre"
                  >
                    Apellido
                  </label>
                  <input
                    type="text"
                    className="shadow appearance-none rounded w-full py-2 px-3 text-gray-700 leading-tight border focus:outline-none focus:ring-2 focus:ring-purple-600 focus:border-transparent"
                    id="lastname"
                    name="lastname"
                    placeholder="Apellido"
                    value={formik.values.lastname}
                    onChange={formik.handleChange}
                    onBlur={formik.handleBlur}
                  />

                  {formik.touched.lastname && formik.errors.lastname ? (
                    <div className="my-2 bg-gray-200 border-l-4 border-red-500 text-red-700 p-4">
                      <p className="font-bold">Error</p>
                      <p>{formik.errors.lastname} </p>
                    </div>
                  ) : null}
                </div>

                <div className="mb-4">
                  <label
                    className="block text-black text-sm font-bold mb-2"
                    htmlFor="nombre"
                  >
                    Nombre de usuario
                  </label>
                  <input
                    type="text"
                    className="shadow appearance-none rounded w-full py-2 px-3 text-gray-700 leading-tight border focus:outline-none focus:ring-2 focus:ring-purple-600 focus:border-transparent"
                    id="username"
                    name="username"
                    placeholder="Apellido"
                    value={formik.values.username}
                    onChange={formik.handleChange}
                    onBlur={formik.handleBlur}
                  />

                  {formik.touched.username && formik.errors.username ? (
                    <div className="my-2 bg-gray-200 border-l-4 border-red-500 text-red-700 p-4">
                      <p className="font-bold">Error</p>
                      <p>{formik.errors.username} </p>
                    </div>
                  ) : null}
                </div>

                <div className="mb-4">
                  <label
                    className="block text-black text-sm font-bold mb-2"
                    htmlFor="email"
                  >
                    Email
                  </label>
                  <input
                    type="email"
                    className="shadow appearance-none rounded w-full py-2 px-3 text-gray-700 leading-tight border focus:outline-none focus:ring-2 focus:ring-purple-600 focus:border-transparent"
                    id="email"
                    name="email"
                    placeholder="Email de Usuario"
                    value={formik.values.email}
                    onChange={formik.handleChange}
                    onBlur={formik.handleBlur}
                  />
                  {formik.touched.email && formik.errors.email ? (
                    <div className="my-2 bg-gray-200 border-l-4 border-red-500 text-red-700 p-4">
                      <p className="font-bold">Error</p>
                      <p>{formik.errors.email} </p>
                    </div>
                  ) : null}
                </div>

                <div className="mb-4">
                  <label
                    className="block text-black text-sm font-bold mb-2"
                    htmlFor="password"
                  >
                    Password
                  </label>
                  <input
                    type="password"
                    className="shadow appearance-none rounded w-full py-2 px-3 text-gray-700 leading-tight border focus:outline-none focus:ring-2 focus:ring-purple-600 focus:border-transparent"
                    id="password"
                    name="password"
                    placeholder="Password de Usuario"
                    value={formik.values.password}
                    onChange={formik.handleChange}
                    onBlur={formik.handleBlur}
                  />
                  {formik.touched.password && formik.errors.password ? (
                    <div className="my-2 bg-gray-200 border-l-4 border-red-500 text-red-700 p-4">
                      <p className="font-bold">Error</p>
                      <p>{formik.errors.password} </p>
                    </div>
                  ) : null}
                </div>

                <input
                  type="submit"
                  className="bg-blue-500 hover:bg-gray-900 w-full p-2 text-white uppercase font-bold"
                  value="Crear Cuenta"
                />
              </form>
            </div>
          </div>
        </div>
      </Layout>
    </>
  );
};
export default Crearcuenta;
