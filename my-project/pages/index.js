import { useState, useEffect } from "react";
import OfferBaner from "../components/Banner/OfferBaner";
import CampanyLogos from "../components/Campanies";
import Features from "../components/features";
import NewsLetter from "../components/NewsLetter.js";
import Ourblog from "../components/Ourblog";
import Services from "../components/services";
import SpecialPRoducts from "../components/SpecialProducts.js";
import Spining from "../components/Spining";
import Subbaners from "../components/SubBanner";
import { SubbannerRight, SubbannerLeft } from "../utils/Laptops";
import GlobalStyle, { Container } from "../style/globalStyles";
import BannerSlider from "../components/Banner/bannerSlider";
import Hero from "../components/Banner/Hero";
import Layout from "../components/Layout";
import Navbar from "../components/navbar";

const Index = () => {
  const [spining, setSpining] = useState(true);
  useEffect(() => {
    setSpining(true);
    setTimeout(() => {
      setSpining(false);
    }, 1000);
  }, []);
  return (
    <>
      <Layout>
        <GlobalStyle />
        <Spining spining={spining} />
        <Navbar />
        <Hero>
          <BannerSlider
            title="home page"
            subtitle="welcome our site to get the best products"
          />
        </Hero>
        <Container>
          <Subbaners subbanners={SubbannerRight} />
          <OfferBaner />
          <Features title="Recent Arrival" />
          <Services />
          <SpecialPRoducts />
          <Ourblog />
          <CampanyLogos />
          <Subbaners inverse="true" subbanners={SubbannerLeft} />
        </Container>
        <NewsLetter />
      </Layout>
    </>
  );
};

export default Index;
