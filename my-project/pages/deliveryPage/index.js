import { useState, useEffect } from 'react';
import NewsLetter from '../../components/NewsLetter.js/index.js';
import GlobalStyle, { Container } from '../../style/globalStyles';

import Spining from '../../components/Spining';
import Hero from '../../components/Banner/Hero';
import BannerSlider from '../../components/Banner/bannerSlider';
import PagesContent from '../../components/deliveryComponent/PagesContent';
import Layout from '../../components/Layout.js';
import Navbar from '../../components/navbar';

const Delivery = () => {
	const [spining, setSpining] = useState(true);
	useEffect(() => {
		setSpining(true);
		setTimeout(() => {
			setSpining(false);
		}, 2000);
	}, []);

	return (
		<>
			<Layout title="delivery">
				<GlobalStyle />
				<Spining spining={spining} />
				<Navbar />
				
				<Hero hero='deliverybanner'>
					<BannerSlider
						title='delivery page'
						subtitle='Packages are generally dispatched within 2 days'
					/>
				</Hero>
				<Container>
					<PagesContent
						title='Delivery'
						subtitle='Shipments and returns Your pack shipment'
					/>
				</Container>
				<NewsLetter />
				
				</Layout>
			
		</>
	);
};

export default Delivery;
