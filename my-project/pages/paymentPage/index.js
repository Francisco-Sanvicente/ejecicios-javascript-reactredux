import { useState, useEffect } from "react";
import Layout from "../../components/Layout";
import NewsLetter from "../../components/NewsLetter.js/index.js";
import GlobalStyle, { Container } from "../../style/globalStyles";

import Spining from "../../components/Spining";
import Navbar from "../../components/navbar";
import Hero from "../../components/Banner/Hero";
import BannerSlider from "../../components/Banner/bannerSlider";
import PagesContent from "../../components/deliveryComponent/PagesContent";

const LegalPage = () => {
	const [spining, setSpining] = useState(true);
	useEffect(() => {
		setSpining(true);
		setTimeout(() => {
			setSpining(false);
		}, 2000);
	}, []);

	return (
		<>
			<Layout title="Payment">
				<GlobalStyle />

				<Spining spining={spining} />
				<Navbar />
				<Hero hero="paymentbanner">
					<BannerSlider
						title="secure payment page"
						subtitle="we use SSL for securing your payments"
					/>
				</Hero>
				<Container>
					<PagesContent
						title="Secure payment"
						subtitle="Our secure payment With SSL"
					/>
				</Container>
				<NewsLetter />
			</Layout>
		</>
	);
};

export default LegalPage;
