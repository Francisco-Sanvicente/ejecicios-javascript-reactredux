import { useState, useEffect } from 'react';
import Layout from "../../components/Layout";
import Navbar from "../../components/navbar";
import Features from '../../components/features';
import NewsLetter from '../../components/NewsLetter.js/index.js';
import GlobalStyle, { Container } from '../../style/globalStyles';
import Spining from '../../components/Spining';
import Hero from '../../components/Banner/Hero';
import BannerSlider from '../../components/Banner/bannerSlider';

const LaptopDetail = () => {
	const [spining, setSpining] = useState(true);
	useEffect(() => {
		setSpining(true);
		setTimeout(() => {
			setSpining(false);
		}, 2000);
	}, []);

	return (
		<>
		
				<Layout title="Detalles">
				<GlobalStyle />
				<Spining spining={spining} />
				<Navbar />
				<Hero hero='detailbanner'>
					<BannerSlider
						title='laptops page'
						subtitle='find the laptop that suits your needs'
					/>
				</Hero>
				<Container>
					<Features title='You Might Also Like' load='true' />
				</Container>
				<NewsLetter />
				</Layout>
		
		</>
	);
};

export default LaptopDetail;
