import { useState, useEffect, useContext } from 'react';
import { useRouter } from 'next/router';
import Link from 'next/link';
import { cartContext } from '../../components/context'
import Layout from "../../components/Layout";
import Navbar from "../../components/navbar";
import Features from '../../components/features';
import NewsLetter from '../../components/NewsLetter.js/index.js';
import GlobalStyle, { Container } from '../../style/globalStyles';
import DetailBaner from '../../components/DetailComponents/DetailBaner';
import Spining from '../../components/Spining';
import ProductDetail from '../../components/DetailComponents/ProductDetail';

const LaptopDetail = () => {
	const [spining, setSpining] = useState(true);
	const { state } = useContext( cartContext )
	const router = useRouter();
	const { slug } = router.query;
	// useEffect(() => {
	// 	setSpining(true);
	// 	setTimeout(() => {
	// 		setSpining(false);
	// 	}, 3000);
	// }, []);

	const laptop = state?.find((laptop) => laptop.slug === slug);

	
	return (
		<>{laptop ? 
			(<Layout title={laptop.slug}>
				<GlobalStyle />
			
				<Navbar />
				<DetailBaner />
				<Container>
					<ProductDetail laptop={laptop} />
					<Features title='You Might Also Like' load='true' />
				</Container>
				<NewsLetter />
				</Layout>
			): 	<Spining spining={spining} />}			
		</>
	);
};

export default LaptopDetail;
