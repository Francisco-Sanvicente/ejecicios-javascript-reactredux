import { ToastContainer } from "react-toastify";
import AuthState from "../components/context/auth/authState";
import CartProvider from "../components/context";
import "tailwindcss/tailwind.css";
import "react-toastify/dist/ReactToastify.css";
import "react-responsive-modal/styles.css";
import "semantic-ui-css/semantic.min.css";
import "../style/index.css";

const MyApp = ({ Component, pageProps }) => {
  return (
    <AuthState>
        <CartProvider>
          <Component {...pageProps} />
          <ToastContainer
            position="top-left"
            autoClose={5000}
            hideProgressBar={false}
            newestOnTop={false}
            closeOnClick
            rtl={false}
            pauseOnFocusLoss
            draggable
            pauseOnHover
          />
        </CartProvider>
    </AuthState>
  );
};
export default MyApp;
