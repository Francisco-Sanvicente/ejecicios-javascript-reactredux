import { useState, useEffect, useContext } from "react";
import { useRouter } from "next/router";
import { useFormik } from "formik";
import * as Yup from "yup";
import { toast } from "react-toastify";
import GlobalStyle from '../style/globalStyles';
import authContext from "../components/context/auth/authContext";
import Spining from "../components/Spining";
import Navbar from "../components/navbar";
import Layout from "../components/Layout";

const Login = () => {
  const [spining, setSpining] = useState(true);
  const router = useRouter();

  const { autenticado, iniciarSesion, } = useContext(authContext)

  useEffect(() => {
    if(autenticado) {
      router.replace('/')
    }
      //eslint-disable-next-line
  }, [autenticado])

  const formik = useFormik({
    initialValues: {
      identifier: "",
      password: "",
    },
    validationSchema: Yup.object({
      identifier: Yup.string()
        .email("El email no es válido")
        .required("El Email es Obligatorio"),
      password: Yup.string().required("El password no puede ir vacio"),
    }),
    onSubmit:  (valores) => {
        iniciarSesion(valores);
        toast.success("Usuario Logeado correctamente");
    },
  });


  useEffect(() => {
    setSpining(true);
    setTimeout(() => {
      setSpining(false);
    }, 1000);
  }, []);

  

  return (
    <>
    
      <Layout title="Login">
        <GlobalStyle />
        <Spining spining={spining} />
  
     <Navbar/>
     
     <div className="md:w-4/5 xl:w-3/5 mx-auto mb-32">
               <h2 className="text-4xl font-sans font-bold text-gray-800 text-center my-4">Iniciar Sesión</h2>
              
               <div className="flex justify-center mt-5">
                   <div className="w-full max-w-lg">
                       <form
                         className="bg-white rounded shadow-md px-8 pt-6 pb-8 mb-4"
                         onSubmit={formik.handleSubmit}
                       >
                         
                           <div className="mb-4">
                               <label 
                                 className="block text-black text-sm font-bold mb-2"
                                 htmlFor="email"
                               >Email</label>
                               <input
                                   type="email"
                                   className="shadow appearance-none rounded w-full py-2 px-3 text-gray-700 leading-tight border focus:outline-none focus:ring-2 focus:ring-purple-600 focus:border-transparen"
                                   id="identifier"
                                   name="identifier"
                                   placeholder="Email de Usuario"
                                   value={formik.values.identifier}
                                   onChange={formik.handleChange}
                                   onBlur={formik.handleBlur}
                               />
                               { formik.touched.identifier && formik.errors.identifier ? (
                                 <div className="my-2 bg-gray-200 border-l-4 border-red-500 text-red-700 p-4">
                                     <p className="font-bold">Error</p>
                                     <p>{formik.errors.identifier} </p>
                                 </div>
                               ) : null }
                           </div>
     
                           <div className="mb-4">
                               <label 
                                 className="block text-black text-sm font-bold mb-2"
                                 htmlFor="password"
                               >Password</label>
                               <input
                                   type="password"
                                   className="shadow appearance-none rounded w-full py-2 px-3 text-gray-700 leading-tight border focus:outline-none focus:ring-2 focus:ring-purple-600 focus:border-transparen"
                                   id="password"
                                   name="password"
                                   placeholder="Password de Usuario"
                                   value={formik.values.password}
                                   onChange={formik.handleChange}
                                   onBlur={formik.handleBlur}
                               />
                               { formik.touched.password && formik.errors.password ? (
                                 <div className="my-2 bg-gray-200 border-l-4 border-red-500 text-red-700 p-4">
                                     <p className="font-bold">Error</p>
                                     <p>{formik.errors.password} </p>
                                 </div>
                               ) : null }
                           </div>
     
                           <input 
                             type="submit"
                             className="bg-blue-500 hover:bg-gray-900 w-full p-2 text-white uppercase font-bold"
                             value="Iniciar Sesión"
                           />
                       </form>
                   </div>
               </div>
             </div>
           
         </Layout>

    </>
  );
};
export default Login;
