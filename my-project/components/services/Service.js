import PropTypes from 'prop-types';
const Service = ({ service }) => {
  return (
    <div className='service-grid'>
      <div className='service-icon flex justify-center'>{service.icon}</div>
      <div className='services-content'>
        <div className='service-title'>{service.title}</div>
        <div className='service-p'>{service.description}</div>
      </div>
    </div>
  );
};
Service.propTypes = {
  service: PropTypes.object.isRequired,
}
export default Service;
