import { ServicesWrapper } from './style';
import { OurService } from '../../utils';
import Service from './Service';

const Services = () => {
  return (
    <ServicesWrapper>
      {OurService.map((service) => (
        <Service key={service.id} service={service} />
      ))}
    </ServicesWrapper>
  );
};
export default Services;
