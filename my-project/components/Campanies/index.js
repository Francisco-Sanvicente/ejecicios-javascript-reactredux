import { useState, useEffect, useRef } from "react";
import Image from 'next/image'
import { LogoWrapper } from "./style";
import { FiChevronLeft, FiChevronRight } from "react-icons/fi";
import { iconsCompanies } from "../../utils"

const CampanyLogos = () => {
  const [width, setWidth] = useState(0);
  const ref = useRef(null);
  useEffect(() => {
    setWidth(window.innerWidth);
  }, []);

  const handleResize = () => {
    setWidth(window.innerWidth);
  };

  useEffect(() => {
    window.addEventListener("resize", handleResize);
    return () => {
      window.removeEventListener("resize", handleResize);
    };
  }, []);

  const nextSlide = () => {
    const slide = ref.current;
    if (width >= 992) {
      slide.scrollLeft += slide.offsetWidth / 2.9;
    } else {
      slide.scrollLeft += slide.offsetWidth / 0.99;
    }
    if (slide.scrollLeft >= slide.scrollWidth - slide.offsetWidth) {
      slide.scrollLeft = 0;
    }
  };
  const previouSide = () => {
    const slide = ref.current;
    if (width >= 992) {
      slide.scrollLeft -= slide.offsetWidth / 2.9;
    } else {
      slide.scrollLeft -= slide.offsetWidth / 0.99;
    }
    if (slide.scrollLeft <= 0) {
      slide.scrollLeft = slide.scrollWidth;
    }
  };

  return (
    <LogoWrapper>
      <div className="slider-content" ref={ref}>
        <div className="logo-slider">
          {iconsCompanies.map((logo) => {
            return (
              <div className="logo-img" key={logo.id}>
                <Image
                  src={logo.icon}
                  alt=""
                  className="logo"
                  width="150%"
                  height="200%"
                  quality={100}  
                  priority={true}
                />
              </div>
            );
          })}
        </div>
      </div>
      <div className="feature-arrows">
        <button className="feature-icon icon-left" onClick={previouSide}>
          <FiChevronLeft className="icon" />
        </button>
        <button className="feature-icon icon-right" onClick={nextSlide}>
          <FiChevronRight className="icon" />
        </button>
      </div>
    </LogoWrapper>
  );
};

export default CampanyLogos;
