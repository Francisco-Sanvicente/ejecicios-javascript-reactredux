import styled, { keyframes } from 'styled-components';
import { colors } from '../../utils/colors';

const swing = keyframes`
  20% {
    transform: rotate3d(0, 0, 1, 15deg);
  }

  40% {
    transform: rotate3d(0, 0, 1, -10deg);
  }

  60% {
    transform: rotate3d(0, 0, 1, 5deg);
  }

  80% {
    transform: rotate3d(0, 0, 1, -5deg);
  }

  to {
    transform: rotate3d(0, 0, 1, 0deg);
  }
`;
export const LogoWrapper = styled.div`
margin-bottom: 2rem;
width: 100%;
position: relative;
overflow: hidden;

.slider-content {
    width: 100%;
    overflow: hidden;
}
.logo-slider {
    padding: 0 1rem;
    display: flex;
    height: 100%;
    align-items: center;
    width: 150%;
    flex-shrink: 0;
    .logo-img {
        width: 17%;
        flex-shrink: 0;
    }
    img {
        width: 40%;
        flex-shrink: 0;
        :hover {
            animation: ${swing} 0.8s linear;
        }
    }
}

.feature-icon {
    padding: 0.3rem 0.5rem;
    background: transparent;
    border: 0.5px solid rgba(0, 0, 0, 0.2);
    border-radius: 2px;
    position: absolute;
    top: 35%;

    :hover {
        background: ${colors.camea};
        border: none;
    }
    :active,
    :focus {
        outline: none;
    }
}
.icon-left {
    left: 0;
}
.icon-right {
    right: 0;
}
`;