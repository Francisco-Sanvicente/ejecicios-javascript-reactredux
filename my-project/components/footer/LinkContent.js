import PropTypes from 'prop-types';

const LinkContent = ({ content }) => {
  
  const { icon, text } = content;

  return (
    <>
      {icon ? (
        <div className='last-link'>
          <div className='inner-icon'>{content.icon}</div>
          <a href='' className='inner-items inner-item'>
            {text}
          </a>{' '}
        </div>
      ) : (
        <div className='inner-links'>
          <a href='' className='inner-items'>
            {text}
          </a>{' '}
        </div>
      )}
    </>
  );
};
LinkContent.propTypes = {
  content: PropTypes.object.isRequired,
}
export default LinkContent;
