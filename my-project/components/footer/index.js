import { FooterWrapper, UpperFooterWrapper, LowerFooter } from './style'
import { FooterList, SocialMedia, PaymentBrands } from '../../utils';
import FooterLink from './FooterLink';
import { Container } from '../../style/globalStyles';

const Footer = () => {
  return (
    <>
      <FooterWrapper>
        <Container>
          <UpperFooterWrapper>
            {FooterList.map((footerLinks) => (
              <FooterLink key={footerLinks.id} footerLinks={footerLinks} />
            ))}
          </UpperFooterWrapper>
          <LowerFooter>
            <div className='lower-content'>
              <div className='soical-media'>
                <ul>
                  {SocialMedia.map((meida) => {
                    return (
                      <li className='social-items' key={meida.id}>
                        <a href='#' className='social-link'>
                          {meida.icon}
                        </a>
                      </li>
                    );
                  })}
                </ul>
              </div>
              <div className='copyRight copy-one'>
                © 2021 - Ecommerce software by Francisco Sanvicente
              </div>
              <div className='payment-methods'>
                <ul>
                  {PaymentBrands.map((payment) => {
                    return (
                      <li className='payment-items' key={payment.id}>
                        <a href='#' className='payment-link'>
                          {payment.icon}
                        </a>
                      </li>
                    );
                  })}
                </ul>
              </div>
              <div className='copyRight copy-two'>
                © 2021 - Ecommerce software by Francisco Sanvicente
              </div>
            </div>
          </LowerFooter>
        </Container>
      </FooterWrapper>
    </>
  );
};
export default Footer;
