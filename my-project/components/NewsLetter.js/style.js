import styled from 'styled-components';
import { colors } from '../../utils/colors';

export const NewsWrapper = styled.div`
background: ${colors.camea};
padding: 1.3rem 0;

.news-content {
    color: #fff;
    text-transform: capitalize;

    .news-letter {
        font-size: 15px;
        font-weight: 300;
    }
    .subscribe {
        font-size: 20px;
        letter-spacing: 1px;
        margin: 0.2rem 0 0.4rem;
    }

    .news-text {
        letter-spacing: 1px;
        font-size: 13px;
        font-weight: 300;
    }
}

form {
    display: flex;
    margin-top: 1rem;
    background: #fff;
    width: 97%;
    height: 2.5rem;
    box-sizing: border-box;
    padding-left: 0.5rem;
    border-radius: 0.3rem;

    .from-icon {
        color: ${colors.camea};
        padding: 0.6rem;
    }

    .input-form {
        border: none;
        padding: 0.5rem;

        width: 90%;
    }
    .input-form:focus,
    .input-form:hover {
        border: none;
        outline: none;
    }

    .submit {
        background: #000;
        justify-self: flex-end;
        width: 3.5rem;
        border: none;
        color: #fff;
        border-top-right-radius: 0.3rem;
        border-bottom-right-radius: 0.3rem;
        font-size: 1.7rem;
        padding-top: 0.15rem;
        cursor: pointer;
        :active,
        :focus {
            outline: none;
        }
    }
}

@media screen and (min-width: 992px) {
    .news-content {
        display: flex;
        height: 100%;
        align-items: center;
        justify-content: space-between;
    }
    form {
        width: 35%;
    }
}
@media screen and (min-width: 1200px) {
    .news-content {
        display: flex;
        height: 100%;
        align-items: center;
        justify-content: space-between;
    }
    form {
        width: 45%;
    }
}
`;