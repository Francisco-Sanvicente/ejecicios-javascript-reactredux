import { HiOutlineArrowNarrowRight } from 'react-icons/hi';
import { MdEmail } from 'react-icons/md';
import { toast } from "react-toastify";
import { useForm } from "../../hooks/useForm";
import { Container } from '../../style/globalStyles';
import { NewsWrapper } from './style';
const NewsLetter = () => {
	const { values: { email }, handleInputChange, reset } = useForm()

	const handleSubmit = () => {
		reset()
		toast.success("Mensahe enviado satisfactoriamente")
	};
	return (
		<NewsWrapper>
			<Container>
				<div className='news-content'>
					<div className='news-titles'>
						<h4 className='news-letter'>news letter</h4>
						<h4 className='subscribe'>subscribe now</h4>
					</div>
					<div className='news-text'>
						Get Daily Updates Form Us Write Email ID Below
					</div>
					<form action='' onSubmit={handleSubmit}>
						<div className='from-icon'>
							<MdEmail />
						</div>
						<input
							className='input-form'
							type='email'
							name='email'
							placeholder='Enter your Email adress'
							required='required'
							value={email}
							onChange={handleInputChange}
						/>
						<button className='submit'>
							<HiOutlineArrowNarrowRight />
						</button>
					</form>
				</div>
			</Container>
		</NewsWrapper>
	);
};

export default NewsLetter;
