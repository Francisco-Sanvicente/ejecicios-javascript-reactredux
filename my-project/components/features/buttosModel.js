import PropTypes from 'prop-types';
import { ImCross } from 'react-icons/im';
const ButtonsModel = ({ setOPenmodels, text }) => {
	return (
		<div className='cart-content buttons-content'>
			<div className='title'>{text}</div>
			<div
				className='cross-icon'
				onClick={() => setOPenmodels(false)}
				style={{ cursor: 'pointer' }}
			>
				<ImCross />
			</div>
		</div>
	);
};
ButtonsModel.propTypes = {
    setOPenmodels: PropTypes.func.isRequired,
    text: PropTypes.string.isRequired
}
export default ButtonsModel;
