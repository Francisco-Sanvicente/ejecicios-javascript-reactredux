import PropTypes from 'prop-types';
import { BsEye } from 'react-icons/bs';
import { FiHeart } from 'react-icons/fi';
import { CgMenuLeft } from 'react-icons/cg';
import { FaShoppingCart } from 'react-icons/fa';


const ButtonList = ({
	addCart,
	setOpen,
	setwishList,
	setCompare,
	goDetail,
	id,
}) => {
	return (
		<div className='buttons-list'>
			<button
				className='icon-list flex justify-center items-center'
				onClick={() => {
					setwishList(true);
				}}
			>
				<FiHeart />
			</button>
			<button
				className='icon-list flex justify-center items-center'
				onClick={() => {
					setCompare(true);
				}}
			>
				<CgMenuLeft />
			</button>
			<button className='icon-list flex justify-center items-center' onClick={goDetail}>
				<BsEye />
			</button>
			<button
				className='icon-list flex justify-center items-center'
				onClick={() => {
					setOpen(true);
					addCart(id);
				}}
			>
				<FaShoppingCart />
			</button>
		</div>
	);
};
ButtonList.propTypes = {
	addCart: PropTypes.func.isRequired,
	setOpen: PropTypes.func.isRequired,
	setwishList: PropTypes.func.isRequired,
	setCompare: PropTypes.func.isRequired,
	goDetail: PropTypes.func.isRequired,
	id: PropTypes.string.isRequired,
}
export default ButtonList;
