import { useState, useContext } from 'react';
import PropTypes from 'prop-types';
import { useRouter } from 'next/router';
import Image from 'next/image'
import { MdStar, MdStarHalf } from 'react-icons/md';
import { FeatureWrapper, ButtonModel } from './styles';
import ButtonList from './buttonList';
//import AddedCart from './AddedCart';
import Spining from '../Spining';
import { cartContext } from '../context';
import ButtonsModel from './buttosModel';

const CardFeatures = ({ laptop, load, cartItems}) => {
	const {
		Open,
		addCart,
		setOpen,
		wishList,
		setwishList,
		compare,
		setCompare,
	} = useContext(cartContext);

	const [spining, setSpining] = useState(false);
	const router = useRouter();
	const goDetail = () => {
		if (load) {
			setSpining(true);
			setTimeout(() => {
				setSpining(false);
			}, 1000);
		}
		router.push(`/detailPage/${laptop.slug}`);
	};
	
	return (
		<>
			<Spining spining={spining} />
			<div className='feature-cards'>
				<ButtonList
					setOpen={setOpen}
					id={laptop._id}
					addCart={addCart}
					goDetail={goDetail}
					setwishList={setwishList}
					setCompare={setCompare}
				/>
				<div className='card-content'>
					<div className='feature-top'>
						<Image
							src={laptop.img.url}
							alt='laptop'
							className='image-active'
							width="50%"
							height="100%"
							quality={100}  
							priority={true}
						/>
						<Image
							src={laptop.imgHover.url}
							alt='laptop'
							className='image-hover'
							width="50%"
							height="100%"
							quality={100}  
							priority={true}
						/>
					</div>
					<div className='footer'>
						<div className='footer-top'>
							<div className='stars'>
								<MdStar className='star-checked' />
								<MdStar className='star-checked' />
								<MdStar className='star-checked' />
								<MdStarHalf className='star-checked' />
								<MdStar className='star' />
							</div>
							<div className='price'>${laptop.price}</div>
						</div>

						<div className='footer-title'>{laptop.title}</div>
						<button className='addCart' onClick={goDetail}>
							Go detail
						</button>
					</div>
				</div>
			</div>
			<FeatureWrapper>
				{/* open */}
				<div className={Open ? 'model-cart ' : 'model-cart'}>
				{/* {cartItems.length > 0 &&
					<AddedCart setOpen={setOpen} cartItems={cartItems} />} */}
				</div>
				<div className={wishList ? 'model-cart button-model' : 'model-cart'}>
					<ButtonsModel
						setOPenmodels={setwishList}
						text='You must be logged in to manage your wishlist'
					/>
				</div>
				<div className={compare ? 'model-cart button-model' : 'model-cart'}>
					<ButtonsModel
						setOPenmodels={setCompare}
						text='The product has been added to list compare.'
					/>
				</div>
			</FeatureWrapper>
		</>
	);
};
CardFeatures.propTypes = {
    laptop: PropTypes.object.isRequired,
}
export default CardFeatures;
