import { useState } from "react";
import PropTypes from 'prop-types';
import Image from 'next/image'
import ModalPayment from "./ModalPayment"
import { ImCross } from "react-icons/im";
import Payment from "../Payment";

const CartNav = ({ total, subTotal, cartItems, crashCart, id }) => {
  const [modalIsOpen, setIsOpen] = useState(false);

  const openModal = () => {
    setIsOpen(true);
  }

  const closeModal = () => {
    setIsOpen(false);
  }

  return (
    //
    <div className="cart-wrap">
      <div className="cart-top-container">
        {cartItems.map((item) => {
          return (
            <div className="cart-top" key={item.id}>
              <div className="img-container">
                <Image src={item.img.url} width='100%' height="100%" alt="laptop"/>
              </div>
              <div className="title-content">
                <div className="title">
                  <span style={{ fontSize: "15px" }}>{item.quantity} </span> X{" "}
                  {item.title}
                </div>
                <div className="price">{item.total}</div>
              </div>
              <div className="close-btn">
                <ImCross
                  className="close-icon"
                  onClick={() => {
                    crashCart(id);
                  }}
                />
              </div>
            </div>
          );
        })}
      </div>
      <div className="cart-items">
        <div className="items">
          <div className="item">{cartItems.length} items</div>
          <div className="orginal-price">{subTotal}</div>
        </div>
        <div className="items">
          <div className="shipping">Shipping</div>
          <div className="shipping">free</div>
        </div>
      </div>
      <div className=" cart-items">
        <div className="shipping">Total (tax incl.)</div>
        <div className="total">{total}</div>
      </div>
      <button onClick={openModal} className="viewCart">
        view Cart
      </button>
      <ModalPayment open={modalIsOpen} close={closeModal}>
        <Payment total={total} crashCart={crashCart} Id={id}/>
      </ModalPayment>
    </div>
  );
};
CartNav.propTypes = {
  total: PropTypes.number.isRequired,
  subTotal: PropTypes.number.isRequired,
  cartItems: PropTypes.array.isRequired,
  crashCart: PropTypes.func.isRequired,
  id: PropTypes.string,
}
export default CartNav;

