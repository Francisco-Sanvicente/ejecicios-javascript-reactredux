import styled, { keyframes } from "styled-components";
import { colors } from "../../utils/colors";

const fixedAnimation = keyframes`
0% {
		top: -100%
	}
100% { 
		top: 0
	}

`;

export const UpperNavWraper = styled.div`
  background: ${colors.camea};

  .upper-nav {
    height: 2.3rem;
    display: flex;
    align-items: center;
    justify-content: space-between;

    @media (max-width: 768px) {
      justify-content: flex-end;
    }
    .work-ours {
      @media (max-width: 768px) {
        display: none;
      }
    }

    .upper-nav-right {
      display: flex;
      align-items: center;

      svg {
        color: black;
        margin-left: 20px;
        display: inline-block;
      }
    }
    .nav-search {
      margin-right: 1rem;
      color: #fff;
      align-self: center;
    }
    .nav-input {
      background-color: transparent;
      border: 0;
      border-bottom: 4px solid #000;
      border-radius: 0;
      color: black;
      padding: 5px 0;
      width: 220px;
      font-size: 20px;
      transition: width 0.7s;
      padding-right: 0px;
      outline: none;
      @media (max-width: 800px) {
        width: 190px;
      }
      @media (max-width: 600px) {
        width: 170px;
      }
      @media (max-width: 468px) {
        width: 100px;
        border-bottom: 2px solid #000;
        font-size: 14px;
      }
      @media (max-width: 368px) {
        width: 50px;
      }
      &:focus {
        width: 350px;
        @media (max-width: 800px) {
          width: 300px;
        }
        @media (max-width: 600px) {
          width: 230px;
        }
        @media (max-width: 468px) {
          width: 160px;
          border-bottom: 2px solid #000;
        }
        @media (max-width: 368px) {
          width: 90px;
        }
      }
    }
    .boton {
      width: 120%;
      background-color: #3490dc;
      border: none;
      outline: none;
      height: 39px;
      border-radius: 9px;
      color: #fff;
      text-transform: uppercase;
      font-weight: 300;
      margin: 10px 0;
      cursor: pointer;
      transition: All 1.5s;
      &:hover {
        background-color: #1121fd;
        color: #000000;
      }
    }
    p {
      font-size: 11px;
      text-transform: capitalize;
      color: #fff;
    }
  }
`;

export const LowerNavWrapper = styled.div`
  .overlay {
		display: block;
		background-color: rgba(0, 0, 0, 0.6);
		z-index: 2;
		position: fixed;
		top: 0;
		left: 0;
		right: 0;
		bottom: 0;
	}
	.nav-wrap {
		display: flex;
		align-items: center;
		width: 100%;
		height: 3.5rem;
		z-index: 100;
		background: #fff;
		transition: all 5s ease;
	}
	.nav-sticky {
		position: fixed;
		top: 0;
		left: 0;
		width: 100%;
		animation: ${fixedAnimation} 0.5s ease;
		box-shadow: 0 4px 16px rgba(0, 0, 0, 0.15);
	}
	.nav {
		display: flex;
		width: 100%;
		justify-content: space-between;
		height: 100%;
		align-items: center;
	}
	.nav-barnd {
		display: flex;
		height: 100%;
		color: #000;
		text-decoration: none;
		align-items: center;
    .brand-text{
      font-size: 150%;
      margin-top: 0;
    }
		span {
			color: ${colors.camea};
		}
	}
	.img-logo {
		align-self: center !important;
		width: 3rem;
		height: 1.8rem;
	}
  .nav-ul {
    display: none;
  }
  .active {
    color: ${colors.camea};
  }
  .nav-ul-sm {
    position: fixed;
    top: 0;
    left: 0;
    display: flex;
    flex-direction: column;
    background: #fff;
    height: 100vh;
    opacity: 0;
    width: 0;
    margin: 0;
    padding: 1rem 0 1rem 1rem;
    overflow: auto;
    z-index: 1000;
    .cross-menue {
      display: block;
      border-bottom: 1px solid #e3e3e3;
      display: flex;
      align-items: center;

      padding-bottom: 0.5rem;

      p {
        text-transform: uppercase;
        margin-left: 0.5rem;
        font-weight: 500;
        font-size: 13px;
      }
    }

    .nav-items {
      list-style: none;
      padding-top: 0.7rem;
    }

    .nav-links {
      text-decoration: none;
      text-transform: capitalize;
      font-size: 13px;
      color: #000;
      :hover {
        color: ${colors.camea};
      }
    }
    .active {
      color: ${colors.camea};
    }
  }
  .right-icons {
    display: flex;
  }

  .icon-right {
    width: 1.5rem;
    height: 1.5rem;
  }
  .icon-menue {
    margin-right: 1rem;
    @media (min-width: 801px) {
      display: none;
    }
  }
  .toggle {
    width: 50%;
    opacity: 1;
    z-index: 10;
  }
  .cart {
    position: relative;
    cursor: pointer;
  }
  .batch {
    position: absolute;
    background: ${colors.camea};
    padding: 0 0.2rem;
    top: -0.2rem;
    right: -0.2rem;
    font-size: 10px;
    color: #fff;
    border-radius: 100%;
  }
  .cart-nav {
    display: block;
    position: absolute;
    right: 0;
    margin-top: 1.5rem;
    background: #fff;
    z-index: 100;
    width: 18rem;
    height: 23rem;
    border: solid 2px ${colors.camea};

    .cart-top-container {
      height: 7.5rem;
      overflow-y: auto;
      padding: 0.7rem;
      margin-right: 1px;

      ::-webkit-scrollbar {
        display: block;
        width: 8px;
      }
      ::-webkit-scrollbar-thumb {
        background-color: rgba(0, 0, 0, 0.2);
      }
    }
    .cart-top {
      margin-bottom: 0.7rem;
      display: flex;
      justify-content: space-between;

      .img-container {
        padding: 1rem 0.3rem;
        border-radius: 0.3rem;
        background: ${colors.lightGrey};
        border: 1px solid #ebebeb;
        img {
          width: 4.5rem;
        }
      }

      .title-content {
        margin-left: 1rem;
        font-size: 13px;

        .title {
          line-height: 1.3rem;
          margin-bottom: 0.3rem;
        }
        .price {
          font-size: 16px;
          color: ${colors.camea};
          font-weight: 700;
        }
      }
      .close-btn {
        color: ${colors.grey};
        cursor: pointer;
      }
    }

    .cart-items {
      padding: 1rem 0.7rem;
      border-bottom: 1px solid #e3e3e3;
      font-size: 13px;
      font-weight: 600;
      letter-spacing: 1px;
      .items {
        display: flex;
        line-height: 1.7rem;
        justify-content: space-between;
      }
    }
    .main-items {
    }
    .viewCart {
      float: right;
      margin-right: 0.7rem;
      margin-top: 1rem;
      font-size: 15px;
      width: 8rem;
      height: 2.5rem;
      border: none;
      border-radius: 0.3rem;
      background: ${colors.camea};
      color: #fff;
      text-transform: capitalize;
    }
  }
  .hide {
    display: none;
  }
  .nav-ul-sm::-webkit-scrollbar {
    display: block;
    width: 8px;
  }

  @media screen and (min-width: 801px) {
    .nav-wrap {
      height: 5rem;
    }

    .nav-ul-sm {
      display: none;
    }
    .overlay {
      display: none;
    }

    .nav-ul {
      position: static;
      margin: 0;
      padding: 0;
      display: flex;

      .cross-menue {
        display: none;
      }
      .nav-items {
        list-style: none;
        padding-left: 2rem;
      }
      .nav-links {
        text-decoration: none;
        text-transform: capitalize;
        font-size: 13px;
        font-weight: 500;
        color: #000;
        :hover {
          color: ${colors.camea};
        }
      }
      .active {
        color: ${colors.camea};
      }
    }
  }
`;

export const customEnterOverlayAnimation = keyframes`
    0% {
      transform: scale(0);
    }
    100% {
      transform: scale(1);
    }
`
export const customLeaveOverlayAnimation = keyframes`
   0% {
      transform: scale(1);
    }
    100% {
      transform: scale(0);
    }
`
export const customEnterModalAnimation = keyframes`
   0% {
      transform: scale(0.2);
    }
    100% {
      transform: scale(1);
    }
  }
`
export const customLeaveModalAnimation = keyframes`
    0% {
      transform: scale(1);
    }
    100% {
      transform: scale(0.2);
    }
`