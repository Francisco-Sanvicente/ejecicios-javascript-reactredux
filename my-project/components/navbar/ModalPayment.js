import PropTypes from 'prop-types';
import { Modal } from 'react-responsive-modal';
import { loadStripe } from "@stripe/stripe-js";
import { Elements } from "@stripe/react-stripe-js";

const ModalPayment = ({ children, open, close }) => {
  const stripePromise = loadStripe(process.env.STRIPE_TOKEN);
  return (
    <Modal
      open={open}
      onClose={close}
      classNames={{
        overlayAnimationIn: 'customEnterOverlayAnimation',
        overlayAnimationOut: 'customLeaveOverlayAnimation',
        modalAnimationIn: 'customEnterModalAnimation',
        modalAnimationOut: 'customLeaveModalAnimation',
        overlay: 'customOverlay',
        modal: 'customModal',
      }}
      animationDuration={800}
    >
      <Elements stripe={stripePromise}>
        {children}
      </Elements>
    </Modal>
  )
}
ModalPayment.propTypes = {
  children: PropTypes.element.isRequired,
  open: PropTypes.bool.isRequired,
  close: PropTypes.func.isRequired,
}
export default ModalPayment
