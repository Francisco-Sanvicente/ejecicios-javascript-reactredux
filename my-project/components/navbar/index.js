import { useState, useEffect, useContext, useRef } from "react";
import { useRouter } from "next/router";
import Link from "next/link";
import Image from "next/image";
import { cartContext } from "../context";
import authContext from "../context/auth/authContext";
import { Container } from "../../style/globalStyles";
import { LowerNavWrapper, UpperNavWraper } from "./style";
import { FaSearch, FaShoppingCart, FaBars } from "react-icons/fa";
import { ImCross } from "react-icons/im";
import Navitems from "./navItems";
import { Navheader, togglebar } from "../../utils";
import CartNav from "./cartNav";
import NavToggle from "./navToggle";

const Navbar = () => {
  const [toggle, setToggle] = useState(false);
  const [navStick, setNavStick] = useState(false);
  const [navCart, setNavcart] = useState(false);
  const [searchStr, setSearchStr] = useState("");
  const [load, setLoad] = useState(false);
  const { cartItems, total, subTotal, crashCart } = useContext(cartContext);
  const { autenticado, logout, usuario } = useContext(authContext);
  const ref = useRef(null);
  const router = useRouter();

  useEffect(() => {
    if (navCart) {
      const handleSidebarRight = (e) => {
        if (ref.current && !ref.current.contains(e.target)) {
          setNavcart(false);
        }
      };
      document.addEventListener("click", handleSidebarRight);
      return () => {
        // Unbind the event listener on clean up
        document.removeEventListener("click", handleSidebarRight);
      };
    }
  });
  useEffect(() => {
    window.addEventListener("scroll", navbarScroll);
    return () => {
      window.removeEventListener("scroll", navbarScroll);
    };
  });
  const navbarScroll = () => {
    if (window.scrollY > 300) {
      setNavStick(true);
    } else {
      setNavStick(false);
    }
  };

  useEffect(() => {
    if (load) {
      router.push(`/search?query=${searchStr}`);
    }
    setLoad(true);
    //eslint-disable-next-line
  }, [searchStr]);

  return (
    <>
      <UpperNavWraper>
        <Container>
          {/* upper NAv */}
          <div className="upper-nav">
            <p className="work-ours">we are Open 24/7</p>
            <div className="upper-nav-right">
              <div className="nav-search">
                <input
                  id="search-game"
                  className="nav-input"
                  value={router.query.query}
                  onChange={(e) => setSearchStr(e.target.value)}
                />
                <FaSearch />
              </div>

              {!autenticado ? (
                <>
                  <Link href="/login">
                    <a className="bg-red-600 hover:bg-red-700 focus:outline-none px-1 py-1 cursor-pointer rounded-lg text-white font-medium focus:ring-2 focus:ring-purple-600 focus:ring-opacity-50 ">
                      Iniciar Seción
                    </a>
                  </Link>
                  <Link href="/crearcuenta">
                    <a className="bg-green-700 hover:bg-green-800 focus:outline-none px-1 py-1 cursor-pointer rounded-lg text-white font-medium focus:ring-2 focus:ring-purple-600 focus:ring-opacity-50 ">
                      Crear Cuenta
                    </a>
                  </Link>
                </>
              ) : (
                <a
                  onClick={logout}
                  className="bg-green-700 hover:bg-green-800 focus:outline-none px-1 py-1 cursor-pointer rounded-lg text-white font-medium focus:ring-2 focus:ring-purple-600 focus:ring-opacity-50 "
                >
                  Cerrar Seción
                </a>
              )}
            </div>
          </div>
        </Container>
      </UpperNavWraper>
      {/* lower nav */}
      <LowerNavWrapper>
        <div className={navStick ? "nav-wrap nav-sticky" : "nav-wrap"}>
          <Container>
            <div className="nav ">
              <Link href="/">
                <a className="nav-barnd">
                  <Image
                    src="https://res.cloudinary.com/franciscosanvicente/image/upload/v1628214063/logo_b53bt4.png"
                    alt=""
                    width="80%"
                    height="35%"
                    className="img-logo"
                    quality={100}  
							      priority={true}
                  />
                  <h1 className="brand-text">
                    S<span>t</span>ore
                  </h1>
                </a>
              </Link>
              {toggle && <div className="overlay"></div>}
              <ul className="nav-ul">
                {Navheader.map((navbar) => (
                  <Navitems key={navbar.id} navbar={navbar} />
                ))}
                {usuario ? (
                  <li className="mx-8 px-5 rounded-lg border-purple-800 bg-purple-600 hover:bg-purple-700 focus:outline-none focus:ring-2 focus:ring-purple-600 focus:ring-opacity-50">
                    {usuario}
                  </li>
                ) : null}
              </ul>
              <ul className={toggle ? "nav-ul-sm toggle" : "nav-ul-sm"}>
                <div className="cross-menue" onClick={() => setToggle(false)}>
                  <ImCross />
                  <p>menue</p>
                </div>
                {Navheader.map((navbar) => (
                  <Navitems key={navbar.id} navbar={navbar} />
                ))}
                {togglebar.map((navbar) => (
                  <NavToggle key={navbar.id} navbar={navbar} />
                ))}
              </ul>

              <div className="right-icons">
                <FaBars
                  className="icon-right icon-menue"
                  onClick={() => setToggle(true)}
                />
                {autenticado ? (
                  <div className="cart" onClick={() => setNavcart(!navCart)}>
                    <FaShoppingCart className="icon-right icon-cart" />
                    <div className="batch">{cartItems.length}</div>
                    {cartItems.length > 0 && (
                      <div className={navCart ? "cart-nav" : "hide"} ref={ref}>
                        <CartNav
                          cartItems={cartItems}
                          total={total}
                          subTotal={subTotal}
                          crashCart={crashCart}
                          id={cartItems.id}
                        />
                      </div>
                    )}
                  </div>
                ) : null}
              </div>
            </div>
          </Container>
        </div>
      </LowerNavWrapper>
    </>
  );
};

export default Navbar;
