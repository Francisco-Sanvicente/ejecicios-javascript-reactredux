import PropTypes from 'prop-types';
import Link from 'next/link';
const NavToggle = ({ navbar }) => {
	return (
		<li className='nav-items'>
			<Link href='/detailPage'>
				<a className='nav-links'>{navbar.title}</a>
			</Link>
		</li>
	);
};
NavToggle.propTypes = {
    navbar: PropTypes.object.isRequired,
}
export default NavToggle;
