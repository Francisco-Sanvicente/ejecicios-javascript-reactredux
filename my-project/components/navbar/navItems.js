import PropTypes from 'prop-types';
import { useRouter } from 'next/router';
import Link from 'next/link';

const Navitems = ({ navbar }) => {
	const router = useRouter();
	// const handleClick = (e) => {
	// 	e.preventDefault()
	// 	router.push(navbar.link)
	//   }
	return (
		<>
			<li className='nav-items'>
				<Link href={navbar.link} >
					<a
						// onClick={handleClick}
						className={
							router.pathname == navbar.link
								? ' nav-links active'
								: ' nav-links'
						}
					>
						{navbar.title}
					</a>
				</Link>
			</li>
		</>
	);
};
Navitems.propTypes = {
    navbar: PropTypes.object.isRequired,
}
export default Navitems;
