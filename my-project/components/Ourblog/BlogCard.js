import PropTypes from 'prop-types';
import Image from "next/image";
import { FaCogs, FaSearch, FaRegCalendarAlt } from 'react-icons/fa';
import { FiHeart } from 'react-icons/fi';

const BlogCard = ({ blog }) => {
  return (
 
    <div className='blog-card'>
      <div className='card-top'>
        <Image src={blog.img.url} alt='' className='img-top' layout="fill" quality={100}	priority={true}/>
        <FaSearch className='search-icon' />
        <FaCogs className='cogs-icon' />
      </div>
      <div className='blog-content'>
        <div className='blog-title'>{blog.title}</div>
        <div className='blog-p'>
          {blog.text}
          <a href='#'>Read more</a>
        </div>
      </div>
      <div className='blog-likes'>
        <div className='date'>
          <FaRegCalendarAlt />
          <p>{blog.date}</p>
        </div>
        <div className='likes'>
          <FiHeart />
          <p>{blog.hit} hit</p>
        </div>
      </div>
    </div>


  );
};
BlogCard.propTypes = {
  blog: PropTypes.object.isRequired,
}
export default BlogCard;
