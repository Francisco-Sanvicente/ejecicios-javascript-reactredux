import PropTypes from 'prop-types';
import { SpiningWrap } from './style';

const Spining = ({ spining }) => {
    return (
      <SpiningWrap>
        <div className={spining ? "spining spining-active" : "spining"}>
          <div className="sk-chase">
            <div className="sk-chase-dot"></div>
            <div className="sk-chase-dot"></div>
            <div className="sk-chase-dot"></div>
            <div className="sk-chase-dot"></div>
            <div className="sk-chase-dot"></div>
            <div className="sk-chase-dot"></div>
          </div>
        </div>
      </SpiningWrap>
    );
  };
  
  Spining.propTypes = {
    spining: PropTypes.bool.isRequired,
}
  export default Spining;