import PropTypes from 'prop-types';
import { SubbanerWrapper } from './style';
import SubCard from './SubCard';

const Subbaners = ({ inverse, subbanners }) => {
	return (
		<>
			<SubbanerWrapper>
				{subbanners.map((subbanner) => (
					<SubCard inverse={inverse} key={subbanner.id} subbanner={subbanner} />
				))}
			</SubbanerWrapper>
		</>
	);
};
Subbaners.propTypes = {
    inverse: PropTypes.string,
    subbanners: PropTypes.array.isRequired
}
export default Subbaners;
