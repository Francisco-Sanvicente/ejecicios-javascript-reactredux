import PropTypes from "prop-types";
import Image from "next/image";
const SubCard = ({ inverse, subbanner }) => {
  return (
    <div className="subcard" inverse={inverse}>
      <div className="subcard-img">
        <Image
          src={subbanner.img}
          alt=""
          width="100%"
          height="80%"  
        />
      </div>
      <div className="subtcard-content">
        <p> {subbanner.pretitle}</p>
        <div className="subcard-title">{subbanner.title}</div>
      </div>
    </div>
  );
};
SubCard.propTypes = {
  inverse: PropTypes.string,
  subbanner: PropTypes.object.isRequired,
};
export default SubCard;
