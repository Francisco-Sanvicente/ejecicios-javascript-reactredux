import { DetailBranerwrapper } from './style';

const DetailBaner = () => {
	return (
		<DetailBranerwrapper>
			<div className='banner-content'>
				<div className='text1'>home</div>
				<div className='text2'>Product detail</div>
			</div>
		</DetailBranerwrapper>
	);
};

export default DetailBaner;
