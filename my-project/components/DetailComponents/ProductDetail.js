import { useContext } from 'react';
import Image from 'next/image'
import PropTypes from 'prop-types';
import { ProductWrapper } from './style';
import { FiHeart } from 'react-icons/fi';
import { CgMenuLeft } from 'react-icons/cg';
import { FiChevronUp, FiChevronDown } from 'react-icons/fi';
import Brands from '../Banner/Brands';
import { brands } from '../../utils';
import { cartContext } from '../context';

const ProductDetail = ({ laptop }) => {
	const { addCart, setOpen, setwishList, setCompare } = useContext(cartContext);
	return (
		<ProductWrapper>
			<div className='cart-top'>
				<div className='brandwrap'>
					<Brands brands={brands} />
				</div>
				<div className='img-wrap'>
					<div className='img-top'>
						<Image
							src={laptop.img.url}
							alt=''
							className='img-top-img'
							width='200%'
							height="200%"
							quality={100}  
						/>
					</div>
				</div>
				<div className='cart-info'>
					<div className='info-title'>{laptop.title}</div>
					<div className='info-content'>
						<div className='info-processor'>
							<strong>processor:</strong> {laptop.processor}
						</div>
						<div className='info-size'>
							<strong>size:</strong> {laptop.size}
						</div>
						<div className='info-ram'>
							<strong>ram:</strong> {laptop.Ram}
						</div>
						<div className='info-storage'>
							<strong>storage:</strong> {laptop.storage}
						</div>
						<div className='description'>
							<strong>description:</strong>
							{laptop.description}
						</div>
					</div>
					<div className='colors-wrap'>
						<div className='color-title'>colors</div>
						<div className='colors'>
							<div
								className='color black'
								style={{ background: 'black' }}
							></div>
							<div
								className='color silver'
								style={{ background: 'silver' }}
							></div>
							<div className='color pink' style={{ background: 'pink' }}></div>
							<div className='color blue' style={{ background: 'blue' }}></div>
						</div>
					</div>
					<div className='price'>
						<div className='real-price'>$300.00</div>
						<div className='net-price'>$250.00</div>
						<div className='discount'>save $5:00</div>
						<div className='tax'>tax included</div>
					</div>
					<div className='buttons'>
						<div className='quantity'>
							<div className='quantity-amount'>1</div>
							<div className='arrows'>
								<div className='arrow-up'>
									<FiChevronUp />
								</div>
								<div className='arrow-down'>
									<FiChevronDown />
								</div>
							</div>
						</div>
						<div className='buttons-list'>
							<button
								className='addCart'
								onClick={() => {
									setOpen(true);
									addCart(laptop.id);
								}}
							>
								add to cart
							</button>
							<button
								className='icon-list flex justify-center items-center'
								onClick={() => {
									setwishList(true);
								}}
							>
								<FiHeart />
							</button>
							<button
								className='icon-list flex justify-center items-center'
								onClick={() => {
									setCompare(true);
								}}
							>
								<CgMenuLeft />
							</button>
						</div>
					</div>
					<div className='instock'>In Stock</div>
				</div>
			</div>
		</ProductWrapper>
	);
};

ProductDetail.propTypes = {
    laptop: PropTypes.array.isRequired,
}
export default ProductDetail;
