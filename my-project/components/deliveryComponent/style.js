import styled from 'styled-components';
import { colors } from '../../utils/colors';

export const ContentWrap = styled.div`
margin: 2rem 0;
.title {
    text-transform: capitalize;
    font-size: 2rem;
    font-weight: 500;
    margin-bottom: 0;
}

.subtitle {
    color: ${colors.grey};
    font-size: 1rem;
    margin-top: 0;
    margin-bottom: 0.7rem;
}
.paragraphs {
    font-size: 13px;
    line-height: 26px;
    color: ${colors.grey};
    letter-spacing: 0.7px;
    font-weight: 400;
}
.p-two {
    margin-top: 1rem;
}
`;