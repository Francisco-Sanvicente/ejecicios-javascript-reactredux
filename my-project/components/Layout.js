import { useContext, useEffect } from "react";
import PropTypes from 'prop-types';
import authContext from "./context/auth/authContext";
import Head from "next/head";
import Footer from "./footer";

const Layout = ({ children, title }) => {
  const { autenticado, usuarioAutenticado, logout } = useContext(authContext);

  useEffect(() => {
    usuarioAutenticado(logout);
    //eslint-disable-next-line
  }, [autenticado]);
  return (
    <>
      <Head>
        <meta property="og:title" content="Ecommerce NEXT.js Strapi App " />
        <meta property="og:description"
          content="Ecommerce Hecha con Next.js como FrotEnd, Strapi como BackEnd, Redux para el manejo del stado global, MongoDB como base de datos y Clodinary como nube especilazada en la subida de imagenes  " />
        <meta name="author" content="Francisco Sanvicente" />
        <meta property="og:image"
          content="https://res.cloudinary.com/franciscosanvicente/image/upload/v1628207979/img-pagina-ecommerce_ipuxx8.png" />
        <meta property="og:url" content="https://ecommerce-next-app.vercel.app/" />
        <meta property="og:type" content="website" />

        <title>Ecommerce | {title}</title>
      </Head>

      <div className="bg-gray-100 min-h-screen">{children}</div>
      <Footer />
    </>
  );
};
Layout.propTypes = {
  title: PropTypes.string,
  //  children: PropTypes.element.isRequired
}
export default Layout;
