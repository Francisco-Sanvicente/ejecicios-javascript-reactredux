import PropTypes from 'prop-types';
import { Slide } from './style';

const Hero = ({hero , children}) => {
    return (
        <Slide>
           <header className={hero}>{children}</header>
        </Slide>
       
    )
}

Hero.defaultProps = {
    hero: "defaultHero"
  };
  Hero.propTypes = {
    hero: PropTypes.string.isRequired,
    children: PropTypes.element.isRequired
}
export default Hero
