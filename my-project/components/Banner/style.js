import styled from 'styled-components';
import { colors } from '../../utils/colors';
import { rotate } from "../SubBanner/keyframes"

export const Slide = styled.div`
	width: 100%;
	.detailbanner,
	.legalbanner,
	.deliverybanner,
	.paymentbanner,
	.defaultHero {
		width: 100%;
		height: 50vh;
		display: flex;
		justify-content: space-between;
		align-items: center;
		text-align: left;
		position: relative;

		::after {
			content: '';
			position: absolute;
			top: 0;
			left: 0;
			width: 100%;
			height: 100%;
			background: rgba(0, 0, 0, 0.4);
		}
	}
	.defaultHero {
		background: center / cover no-repeat url('https://res.cloudinary.com/franciscosanvicente/image/upload/v1628218038/banner_u0ps6b.jpg');
	}
	.detailbanner {
		background: center / cover no-repeat url('https://res.cloudinary.com/franciscosanvicente/image/upload/v1628165536/blog_4_e5ed5d9b64.jpg');
	}
	.deliverybanner {
		background: center / cover no-repeat url('https://res.cloudinary.com/franciscosanvicente/image/upload/v1628218038/delivery_q5f944.jpg');
	}
	.legalbanner {
		background: center / cover no-repeat url('https://res.cloudinary.com/franciscosanvicente/image/upload/v1628165536/small_blog_1_db51dd5ba2.jpg');
	}
	.paymentbanner {
		background: center / cover no-repeat url('https://res.cloudinary.com/franciscosanvicente/image/upload/v1628218056/page-3_y8bujm.jpg');
	}
	.bannerSlide-contnt {
		display: flex;
		flex-direction: column;
		justify-content: center;
		margin-left: 1.5rem;
		width: 90%;

		p {
			text-transform: uppercase;
			color: #fff;
			font-size: 12px;
			z-index: 1;
		}
	}
	.banner-title {
		line-height: 1.3;
		font-size: 1.2rem;
		margin-top: 0.5rem;
		margin-bottom: 1rem;
		text-transform: capitalize;
		letter-spacing: 1px;
		z-index: 1;
		color: #fff;
		width: 80%;
	}

	.banner-title span {
		display: block;
	}
	.banner-button {
		text-decoration: none;
		display: block;
		background: ${colors.camea};
		border: none;
		margin-top: 0.7rem;
		text-align: center;
		width: 10rem;
		padding: 0.5rem 1rem;
		z-index: 1;
		font-size: 12px;
		text-transform: uppercase;
		border-radius: 1.5rem;
		color: #fff;
	}
	@media screen and (min-width: 576px) {
		.banner-title {
			font-size: 1.5rem;
		}
	}
	@media screen and (min-width: 768px) {
		.bannerSlide-contnt {
			width: 70%;
		}
		.banner-title {
			font-size: 1.7rem;
		}
	}
	@media screen and (min-width: 992px) {
		.detailbanner,
		.legalbanner,
		.paymentbanner,
		.deliverybanner,
		.defaultHero {
			height: 75vh;
		}

		.banner-title {
			font-size: 2rem;
		}
	}
	@media screen and (min-width: 1200px) {
		.roombanner,
		.defaultHero {
			height: 80vh;
		}
	}
`;

export const BrandWraper = styled.div`
	letter-spacing: 1.3px;
	.brandHeader {
		display: flex;
		align-items: center;
	}
	.list-icon {
		background: ${colors.camea};
		padding: 0.2rem 0.5rem;
		border-radius: 0.2rem;
	}
	.brand-title {
		color: ${colors.camea};
		font-weight: 700;
		font-size: 14px;
		margin-left: 0.5rem;
		margin-top: 1.3rem;
		margin-bottom: 1rem;
	}
	.brands-ul {
		margin: 0;
		padding: 0;
	}
	.brand-items {
		list-style: none;
		margin-bottom: 1rem;
		border-top: 0.1px solid #dcdde1;
		width: 90%;
	}
	.brand-link {
		color: ${colors.grey};
		text-decoration: none;
		font-size: 13px;
		text-transform: capitalize;

		:hover {
			color: ${colors.camea};
		}
	}
`
export const OfferWraper = styled.div`
  background: center / cover no-repeat url('https://res.cloudinary.com/franciscosanvicente/image/upload/v1628165536/blog_2_b081398367.jpg');
  display: flex;
  justify-content: center;
  padding: 1.5rem;
  border-radius: 0.2rem;
  overflow: hidden;
  align-items: center;
  text-align: center;
  position: relative;
  margin-bottom: 2rem;
  height: 5rem;
  ::before {
    content: '';
    position: absolute;
    top: 0;
    left: 0;
    border-radius: 0.2rem;
    width: 100%;
    height: 100%;
    background: rgba(0, 0, 0, 0.6);
  }
  ::after {
    content: '';
    position: absolute;
    top: 0;
    left: 0;
    border-radius: 0.2rem;
    width: 100%;
    height: 100%;
    background: rgba(255, 255, 255, 0.3);
    opacity: 0;
  }
  :hover::after {
    animation: ${rotate} 1s ease;
    opacity: 1;
  }
  .offer-content {
    z-index: 10;
  }
  .offer-content-p {
    display: none;
  }

  .offer-title {
    font-size: 17px;
    color: #fff;
    text-transform: capitalize;
  }
  @media screen and (min-width: 576px) {
    height: 6.5rem;
    .offer-content-p {
      display: block;
      color: ${colors.camea};
      font-size: 13px;
      letter-spacing: 1px;
    }

    .offer-title {
      font-size: 19px;
      word-spacing: 3px;
    }
  }
  @media screen and (min-width: 768px) {
    height: 7rem;
  }
  @media screen and (min-width: 992px) {
    height: 8.5rem;
  }
  @media screen and (min-width: 1200px) {
    height: 12rem;
    .offer-content-p {
      font-size: 15px;
    }

    .offer-title {
      font-size: 28px;
    }
  }
`;