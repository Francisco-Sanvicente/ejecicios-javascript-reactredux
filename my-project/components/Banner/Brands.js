import PropTypes from 'prop-types';
import Link from "next/link";
import { BrandWraper } from './style'
import { FaListUl } from 'react-icons/fa';

const Brands = ({ brands }) => {
	return (
		<BrandWraper>
			<div className='brandHeader'>
				<div className='list-icon'>
					<FaListUl />
				</div>

				<div className='brand-title'>Shop By Categories</div>
			</div>
			<ul className='brands-ul'>
				{brands.map((brand) => {
					return (
						<li className='brand-items' key={brand.id}>
							<Link href='/detailPage' >
								<a className='brand-link'>	{brand.title}</a>
							</Link>
						</li>
					);
				})}
			</ul>
		</BrandWraper>
	);
};
Brands.propTypes = {
    brands: PropTypes.array.isRequired,
}

export default Brands;
