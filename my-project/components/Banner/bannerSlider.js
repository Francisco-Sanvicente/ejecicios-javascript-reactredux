import PropTypes from 'prop-types';
import { Container } from '../../style/globalStyles';
import Link from 'next/link';
import { brands } from '../../utils';
import Brands from './Brands';

const BannerSlider = ({ title, subtitle }) => {
	return (
		<>
			<Container>
				<div className='bannerwraper'>
					<div className='brandwrap'>
						<Brands brands={brands} className='brands' />
					</div>
					<div className='bannerSlide-contnt'>
						<p>{title}</p>
						<h1 className='banner-title'>{subtitle}</h1>
						<Link href='/detailPage'>
							<a className='banner-button'>explore more</a>
						</Link>
					</div>
				</div>
			</Container>
		</>
	);
};

BannerSlider.propTypes = {
    title: PropTypes.string.isRequired,
    subtitle: PropTypes.string.isRequired
}

export default BannerSlider;
