import { OfferWraper } from './style'
import { rotate } from '../SubBanner/keyframes';

const OfferBaner = () => {
  return (
    <OfferWraper>
      <div className='offer-content'>
        <p className='offer-content-p'>HUGE DISCOUNT ALL OVER $100</p>
        <h2 className='offer-title'>Buy laptops & desks Online</h2>
      </div>
    </OfferWraper>
  );
};

export default OfferBaner;
