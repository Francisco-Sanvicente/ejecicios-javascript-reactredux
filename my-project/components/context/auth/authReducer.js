import { 
    REGISTRO_EXITOSO,
    REGISTRO_ERROR,
    OCULTAR_ALERTA,
    LOGIN_EXITOSO,
    LOGIN_ERROR,
    USUARIO_AUTENTICADO,
    CERRAR_SESION
} from '../../../types';

const authReducer = (state, action) => {
    switch(action.type) {
        case REGISTRO_EXITOSO:
            return{
                ...state,
                autenticado: true,
                usuario: action.payload,
                mensaje: "Registro exitoso"
            }
        case REGISTRO_ERROR:
            return{
                ...state,
                message: "Ha ocurrido un error al registrase"
            }
           
        
        case LOGIN_ERROR:
            return {
                ...state,
                mensaje: "Usuario o contraseña incorrectos"
            }       
        case LOGIN_EXITOSO: 
            
            return {
                ...state,
                token: action.payload,
                autenticado: true,
                message: "Login exitoso"
            }
        case OCULTAR_ALERTA:
            return {
                ...state,
                mensaje: null
            } 
        case USUARIO_AUTENTICADO:
            return {
                ...state,
                usuario: action.payload,
                autenticado: true
            }
        case CERRAR_SESION: 
            localStorage.removeItem('token');
            return {
                ...state,
                usuario: null, 
                token: null,
                autenticado: null,

            }
        default:
            return state;
    }
}
export default authReducer;