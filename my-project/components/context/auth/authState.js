import { useReducer } from "react";
import jwtDecode from "jwt-decode";
import { useRouter } from "next/router";
import authContext from "./authContext";
import authReducer from "./authReducer";
import clienteAxios from "../../../config/axios";
import tokenAuth from "../../../config/tokenAuth";
import {
  REGISTRO_EXITOSO,
  REGISTRO_ERROR,
  OCULTAR_ALERTA,
  LOGIN_EXITOSO,
  LOGIN_ERROR,
  USUARIO_AUTENTICADO,
  CERRAR_SESION,
} from "../../../types";


const AuthState = ({ children }) => {
  // Definir un state inicial
  const initialState = {
    token: typeof window !== "undefined" ? localStorage.getItem("token") : "",
    autenticado: false,
    usuario: null,
    mensaje: null,
    cargando: null,
  };

  // Definir el reducer
  const [state, dispatch] = useReducer(authReducer, initialState);

  const router = useRouter();

  // Registrar nuevos usuarios
  const registrarUsuario = datos => {
    clienteAxios.post('auth/local/register', datos)
      .then((res) => {
        if (res.data.jwt) {
          dispatch({
            type: REGISTRO_EXITOSO,
            payload: res.data.user.username,
          });
          localStorage.setItem("token", res.data.jwt);
        }
      })
      .catch(() => {
        dispatch({
          type: REGISTRO_ERROR
        });
      })
       .finally(() => {
        // Limpia la alerta después de 3 segundos
        setTimeout(() => {
          dispatch({
            type: OCULTAR_ALERTA,
          });
        }, 3000);
      });
  };

  // Autenticar Usuarios
  const iniciarSesion = datos => {
    clienteAxios.post('/auth/local', datos)
      .then((respuesta) => {
        dispatch({
          type: LOGIN_EXITOSO,
          payload: respuesta.data.jwt,
        });
        localStorage.setItem("token", respuesta.data.jwt);
        return respuesta.data;
      })
      .catch(() => {
        dispatch({
          type: LOGIN_ERROR,
        });
      })
       .finally(() => {
        // Limpia la alerta después de 3 segundos
        setTimeout(() => {
          dispatch({
            type: OCULTAR_ALERTA,
          });
        }, 3000);
      });
  };
  const hasExpiredToken = token => {
    const tokenDecode = jwtDecode(token);

    const expireDate = tokenDecode.exp * 1000;

    const currentDate = new Date().getTime();

    if (currentDate > expireDate) {
      return true;
    }

    return false;
  };

  //Retorne el Usuario autenticado en base al JWT
  const usuarioAutenticado = logout => {

    const token = localStorage.getItem("token");
    if (!token) {
      return logout();
    } else {
      if (hasExpiredToken(token)) {
        // Token caducado
        return logout();
      } else {
        tokenAuth(token);
      }
    }

    clienteAxios.get("/users/me")
      .then(respuesta => {
        if (respuesta.data.username) {
          dispatch({
            type: USUARIO_AUTENTICADO,
            payload: respuesta.data.username,
          });
        }
      })

      .catch(() => {
        dispatch({
          type: LOGIN_ERROR,
        });
      })
       .finally(() => {
        // Limpia la alerta después de 3 segundos
        setTimeout(() => {
          dispatch({
            type: OCULTAR_ALERTA,
          });
        }, 3000);
      });
  };

  // Cerrar la sesión
  const logout = () => {
    if (state.autenticado) {
      dispatch({
        type: CERRAR_SESION,
      });
      router.push("/");
    }
  };

  return (
    <authContext.Provider
      value={{
        token: state.token,
        autenticado: state.autenticado,
        usuario: state.usuario,
        mensaje: state.mensaje,
        cargando: state.cargando,
        usuarioAutenticado,
        registrarUsuario,
        iniciarSesion,
        logout,
      }}
    >
      {children}
    </authContext.Provider>
  );
};

export default AuthState;
