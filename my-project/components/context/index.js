import { createContext, useState, useEffect, useContext } from 'react';
export const cartContext = createContext();
import { toast } from "react-toastify";
import authContext from "../context/auth/authContext";
import { useFetch } from '../../hooks/useFetch';

const CartProvider = ({ children }) => {
	const [cartItems, setCartItems] = useState([]);
	const [total, setTotal] = useState(0);
	const [subTotal, setSubtotal] = useState(0);
	const [Open, setOpen] = useState(false);
	const [wishList, setwishList] = useState(false);
	const [compare, setCompare] = useState(false);
	const {autenticado } = useContext(authContext);

	const { state, error, loading }  = useFetch('/productos');

	useEffect(() => {
		addTotals();
   //eslint-disable-next-line
	}, [cartItems]);

	//añadidir productos al carrito

	const addCart = (id) => {
		const tempPRoducts = [...state];
		const product = tempPRoducts.find((item) => item.id === id);

		const cartItem = cartItems.find((item) => item.id === id);
		if (cartItem) {
			
		//	const token = localStorage.getItem("token");
			if(!autenticado){
				toast.warning("Para comprar antes debe iniciar sesión")
			}
			else{
			
				cartItem.quantity += 1
				cartItem.total = cartItem.quantity * cartItem.price;
				setCartItems([...cartItems]),
				toast.success("Producto añadido al carrito")
				
			}			
		} else {
			if(!autenticado){
				toast.warning("Para comprar antes debe iniciar sesión")
			}
			else {
				product.quantity = 1;
				product.total = product.quantity * product.price;
				setCartItems([...cartItems, product]);
				toast.success("Producto añadido al carrito")	
			}
			
		}
	};
		// eliminar productos del carrito
	const crashCart = (id) => {
		const item =  cartItems.filter((item) =>  item.id === id)
		// const cartItem = cartItems.find((item) => item.id == id);
		// const item = delete cartItems[cartItem]
		if (item) {	
			item.quantity -= 1;
			item.total = item.quantity * item.price;
				
				setCartItems([...item]),
				 toast.info("Producto eliminado del carrito ")
						
		} else {
			setCartItems([...cartItems]);
		}
	};


	//
	const addTotals = () => {
		let subTotal = 0;
		cartItems.map((item) => (subTotal += item.total));
		const tempTax = subTotal * 0.02;
		const tax = parseFloat(tempTax.toFixed(2));
		const total = subTotal + tax;
		setSubtotal(subTotal);
		setTotal(total);
	};
	return (
		<div>
			<cartContext.Provider
				value={{
					cartItems,
					total,
					subTotal,
					Open,
					wishList,
					compare,
					addCart,
					setwishList,
					setCompare,
					setOpen,
					state,
					error,
					loading,
					crashCart
				}}
			>
				{children}
			</cartContext.Provider>
		</div>
	);
};

export default CartProvider;
