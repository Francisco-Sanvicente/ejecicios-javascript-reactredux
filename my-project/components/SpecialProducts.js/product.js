import PropTypes from 'prop-types';
import Image from "next/image";
import { MdStar, MdStarHalf } from 'react-icons/md';
import { timeStamps } from '../../utils';
import { BsEye } from 'react-icons/bs';
import { FiHeart } from 'react-icons/fi';
import { CgMenuLeft } from 'react-icons/cg';

const Product = ({ product, carousels }) => {

  return (
    <div className='product'>
      <div className='product-top'>
        <div className='product-img'>
          <Image
            src={product.img.url}
            alt='laptop'
            width='200%'
            height="200%"
            className='image-active'
            quality={100}
            priority={true}
          
          />
        </div>
        <div className='product-carousel'>
          <div className='carousel-slide'>
            {carousels.map(carousel => {
              return (
                <div className='img-carousel' key={carousel._id}>
                  <Image
                    src={carousel.img.url}
                    alt='laptop'
                    className='image-right'
                    width='200%'
                    height="200%"
                    quality={100}
                    priority={true}
                  />
                </div>
              );
            })}
          </div>
        </div>
      </div>
      <div className='product-description'>
        <div className='stars'>
          <MdStar className='star-checked' />
          <MdStar className='star-checked' />
          <MdStar className='star-checked' />
          <MdStarHalf className='star-checked' />
          <MdStar className='star' />
        </div>
        <div className='product-title'>{product.title}</div>
        <div className='product-p'>{product.description}</div>
        <div className='price'>
          <span className='real-price'>{product.price}</span>
          <span className='dis-count'>-5%</span>{' '}
          <span className='net-price'>$82.09</span>
        </div>
        <div className='time'>
          {timeStamps.map(time => {
            return (
              <div className='time-unit' key={time.id}>
                <div
                  className={
                    time.color ? 'time-count time-active' : 'time-count'
                  }
                >
                  {time.count}
                </div>
                <div className='time-title'>{time.title}</div>
              </div>
            );
          })}
        </div>
        <div className='buttons-list'>
          <button className='addCart'>add to cart</button>
          <button className='icon-list flex justify-center items-center'>
            <FiHeart />
          </button>
          <button className='icon-list flex justify-center items-center'>
            <CgMenuLeft />
          </button>
          <button className='icon-list flex justify-center items-center'>
            <BsEye />
          </button>
        </div>
      </div>
    </div>
  );
};
Product.propTypes = {
  product: PropTypes.object.isRequired,
  carousels: PropTypes.array.isRequired
}
export default Product;
