import styled from "styled-components";
import { Button } from "semantic-ui-react";
import { colors } from "../../utils/colors";

export const FormDiv = styled.div`
  background-color: #fff;
  width: 500px;
  padding: 25px;
  margin: 25px auto 0;
  box-shadow: 0px 0px 20px rgba(0, 0, 0, 0.5);
  border-radius: 10px;

  h2 {
    background-color: #fcfcfc;
    color: #2f78ff;
    font-size: 24px;
    padding: 10px;
    margin-bottom: 8px;
    text-align: center;
    border: 1px dashed #2f78ff;
  }
  h4 {
    padding-bottom: 5px;
    color: #2f78ff;
  }
  .input_group {
    margin-bottom: 8px;
    width: 100%;
    position: relative;
    display: flex;
    flex-direction: row;
    padding: 5px 0;
  }
`;
export const FormInputs = styled.div`
  .input_box {
    width: 100%;
    margin-right: 12px;
    position: relative;
    .size{
        width: 80% !important;
    }
}

.input_box:last-child {
    margin-right: 0;
}

.input_box .name {
    padding: 14px 10px 14px 50px;
    width: 100%;
    background-color: #fcfcfc;
    border: 1px solid #0003;
    outline: none;
    letter-spacing: 1px;
    transition: 0.3s;
    border-radius: 3px;
    color: #333;
}

.input_box .name:focus, .dob:focus {
    -webkit-box-shadow: 0 0 2px 1px ${colors.camea};
    -moz-box-shadow: 0 0 2px 1px ${colors.camea};
    box-shadow: 0 0 2px 1px ${colors.camea};
    border: 1px solid ${colors.camea};
}

.input_box .icon {
    width: 48px;
    display: flex;
    justify-content: center;
    align-items: center;
    position: absolute;
    top: 0px;
    left: 0px;
    bottom: 0px;
    color: #333;
    background-color: #f1f1f1;
    border-radius: 2px 0 0 2px;
    transition: 0.3s;
    font-size: 20px;
    pointer-events: none;
    border: 1px solid #00000003;
    border-right: none;
    height:100%;
}

.name:focus+.icon {
    background-color: ${colors.camea};
    color: #fff;
    border-right: 1px solid ${colors.camea};
    border: none;
    transition: 1s;
}

.dob {
    width: 30%;
    padding: 14px;
    text-align: center;
    background-color: #fcfcfc;
    transition: 0.3s;
    outline: none;
    border: 1px solid #c0bfbf;
    border-radius: 3px;
}

.radio {
    display: none;
}

.input_box label {
    width: 50%;
    padding: 13px;
    background-color: #fcfcfc;
    display: inline-block;
    float: left;
    text-align: center;
    border: 1px solid #c0bfbf;
}

.input_box label:first-of-type {
    border-top-left-radius: 3px;
    border-bottom-right-radius: 3px;
    border-right: none;
}

.input_box label:last-of-type {
    border-top-right-radius: 3px;
    border-bottom-right-radius: 3px;
}

.radio:checked+label {
    background-color: ${colors.camea};
    color: #fff;
    transition: 0.5s;
}

`
export const Boton = styled(Button)`

    width: 100%;
    background-color: ${colors.camea};
    border: none;
    color: #fff;
    padding: 25px;
    border-right: 4px;
    font-size: 16px;
    transition: all 0.3s ease;
    border-radius: 8px;

&:hover {
    cursor: pointer;
    background-color:#2f78ff;
}
`