import { useState } from "react";
import PropTypes from "prop-types";
import { CardElement, useStripe, useElements } from "@stripe/react-stripe-js";
import { useFormik, } from "formik";
import * as Yup from "yup";
import axios from "axios";
import Swal from "sweetalert2";
import { Input } from 'semantic-ui-react'
import { FcBusinessman, FcManager, FcFeedback, FcHome } from "react-icons/fc";
import { FormDiv, FormInputs, Boton } from "./style";

const Payment = ({ total, crashCart, Id }) => {
  const stripe = useStripe();
  const elements = useElements();
  const [state, setState] = useState({});
  const [error, setError] = useState(false);
  const [loading, setLoading] = useState(false);

  const formik = useFormik({
    initialValues: {
      name: "",
      tagetName: "",
      email: "",
      adress: "",
    },
    validationSchema: Yup.object({
      name: Yup.string().required(),
      tagetName: Yup.string().required(),
      email: Yup.string().email().required(),
      adress: Yup.string().required(),
    }),
    onSubmit: (valores, { resetForm }) => {
      setLoading(true);
      stripe
        .createPaymentMethod({
          type: "card",
          card: elements.getElement(CardElement),
        })
        .then((res) => setState(res.paymentMethod))
        .catch(() => setError(false));
      if (!error) {
        const { id } = state;
     
        axios
          .post(process.env.URL_BACKEND_STRIPE, {
            amount: 1000, //cents
            id,  
          })
          .then(({ data }) => Swal.fire("Pago Completado", "Pago realizado exitosamente", "success"))
          .catch((error) => Swal.fire("Error", error.message, "error"))
          .finally(() => {
            resetForm()
            elements.getElement(CardElement).clear();
            setLoading(false);
            crashCart(Id);
          })
      }
    },
  });
  return (
    <>
      <FormDiv className="wrapper bg-white w-auto p-6 ">
        <h2>Payment Form</h2>
        <form action="" onSubmit={formik.handleSubmit} method="post">
          <h4>Account</h4>
          <div className="input_group">
            <FormInputs className="input_box">
              <div className="input_box">
                <Input
                  type="text"
                  id="name"
                  name="name"
                  value={formik.values.name}
                  onChange={formik.handleChange}
                  placeholder="Nombre"
                  required
                  className="name size1"
                  error={formik.errors.name}
                  size='small'
                />
                <FcBusinessman className="icon" />
                {/* <i className="fa fa-user icon"></i> */}
              </div>
            </FormInputs>
            <FormInputs className="input_box">
              <div className="input_box">
                <Input
                  type="text"
                  required
                  id="tagetName"
                  name="tagetName"
                  value={formik.values.tagetName}
                  onChange={formik.handleChange}
                  placeholder="Apellido"
                  className="name size"
                  error={formik.errors.tagetName}
                  size='small'
                />
                <FcManager className="icon" />
                {/* <i className="fa fa-user icon"></i> */}
              </div>
            </FormInputs>
          </div>
          <FormInputs className="input_group">
            <div className="input_box">
              <Input
                type="email"
                placeholder="Email Address"
                required
                id="email"
                name="email"
                value={formik.values.email}
                onChange={formik.handleChange}
                className="name"
                error={formik.errors.email}
              />
              <FcFeedback className="icon" />
              {/* <i className="fa fa-envelope icon"></i> */}
            </div>
          </FormInputs>
          <FormInputs className="input_group">
            <div className="input_box">
              <Input
                type="text"
                placeholder="Direccion de envio"
                required
                id="adress"
                name="adress"
                value={formik.values.adress}
                onChange={formik.handleChange}
                className="name"
                error={formik.errors.adress}
              />
              <FcHome className="icon" aria-hidden="true" />
              {/* <i className="fa fa-map-marker icon" aria-hidden="true"></i> */}
            </div>
          </FormInputs>

          <FormInputs className="input_group">
            <div className="input_box">
              <CardElement />
              {/* <i className="fa fa-credit-card icon"></i> */}
            </div>
          </FormInputs>

          <FormInputs className="input_group">
            <div className="input_box">
              <Boton type="submit" loading={loading} disabled={!stripe} color='blue' size='large' fluid>Pagar {total}$</Boton>
            </div>
          </FormInputs>
        </form>
      </FormDiv>
    </>
  );
};
Payment.propTypes = {
  total: PropTypes.number.isRequired,
  crashCart: PropTypes.func.isRequired,
  Id: PropTypes.string,
};
export default Payment;
