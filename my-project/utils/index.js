import Image from "next/image";
import { BiSupport, BiWorld } from 'react-icons/bi';
import { GrDeliver, GrMoney } from 'react-icons/gr';
import { MdLocationOn, MdLocalHospital, MdEmail } from 'react-icons/md';
import {
	FaFax,
	FaFacebookF,
	FaYoutube,
	FaInstagram,
	FaTwitter,
	FaGooglePlusG,
} from 'react-icons/fa';

export const url = process.env.URL_BACKEND;

export const Navheader = [
	{
		id: 1,
		title: 'home',
		link: '/',
	},
	{
		id: 2,
		title: 'laptops',
		link: '/detailPage',
	},
	{
		id: 3,
		title: 'delivery',
		link: '/deliveryPage',
	},

	{
		id: 4,
		title: 'legal active',
		link: '/legalPage',
	},
	{
		id: 5,
		title: 'secure payment',
		link: '/paymentPage',
	},
	
];

export const brands = [
	{
		id: 1,
		title: 'mac os',
	},
	{
		id: 2,
		title: 'sumsong',
	},
	{
		id: 3,
		title: 'toshiba',
	},
	{
		id: 4,
		title: 'mac os',
	},
	{
		id: 5,
		title: 'cell',
	},
	{
		id: 6,
		title: 'lenovo',
	},
	{
		id: 7,
		title: 'hp',
	},
	{
		id: 8,
		title: 'accer',
	},


];

export const togglebar = [
	{
		id: 1,
		title: 'laptop brands',
		link: '/',
	},
	{
		id: 2,
		title: 'mac os',
		link: '/',
	},
	{
		id: 3,
		title: 'toshiba',
		link: '/',
	},

	{
		id: 4,
		title: 'sumsong',
		link: '/',
	},
	{
		id: 5,
		title: 'dell',
		link: '/',
	},
	{
		id: 6,
		title: 'lenova',
		link: '/',
	},
	{
		id: 7,
		title: 'accer',
		link: '/',
	},
];
/* 
'laptop brands',
	'mac os',
	'toshiba',
	'sumsong',
	'dell',
	'lenova',
	'hp',
	'accer',
	'more categories',
	'home',
	'featured',
	'shipment',
	'delivery',
	'legal active',
	'secure payment',
	'more',




*/
export const SocialMedia = [
	{
		id: 1,
		icon: <FaFacebookF />,
	},
	{
		id: 2,
		icon: <FaTwitter />,
	},
	{
		id: 3,
		icon: <FaYoutube />,
	},
	{
		id: 4,
		icon: <FaGooglePlusG />,
	},
	{
		id: 5,
		icon: <FaInstagram />,
	},

];

export const iconsCompanies = [
	{
		id: 1,
		icon: "https://res.cloudinary.com/franciscosanvicente/image/upload/v1628207980/logo-4_mic28q.png",
	},
	{
		id: 2,
		icon: 'https://res.cloudinary.com/franciscosanvicente/image/upload/v1628207976/logo-7_endd6z.png'
	},
	{
		id: 3,
		icon: 'https://res.cloudinary.com/franciscosanvicente/image/upload/v1628207974/logo-3_f9ybo6.png',
	},
	{
		id: 4,
		icon: 'https://res.cloudinary.com/franciscosanvicente/image/upload/v1628207973/logo-6_qhnzb9.png',
	},
	{
		id: 5,
		icon: 'https://res.cloudinary.com/franciscosanvicente/image/upload/v1628207962/logo-1_kgytmp.png',
	},
	{
		id: 6,
		icon: 'https://res.cloudinary.com/franciscosanvicente/image/upload/v1628207961/logo-2_xm4lgr.png',
	},
	{
		id: 7,
		icon: 'https://res.cloudinary.com/franciscosanvicente/image/upload/v1628207954/logo-5_x7bk0w.png',
	},

];

export const PaymentBrands = [
	{
		id: 1,
		icon: 	<Image src='https://res.cloudinary.com/franciscosanvicente/image/upload/v1628218723/visa_yf8v1g.png' alt='visa-card' width={30} height={30} />,
	},
	{
		id: 2,
		icon: 	<Image src='https://res.cloudinary.com/franciscosanvicente/image/upload/v1628218723/maestro_bbagcq.png' alt='maestro-card' width={30} height={30} />,
	},
	{
		id: 3,
		icon: 	<Image src='https://res.cloudinary.com/franciscosanvicente/image/upload/v1628218723/american_express_ywhiuo.png' alt='american_express-card' width={30} height={30} />,
	},
	{
		id: 4,
		icon: <Image src='https://res.cloudinary.com/franciscosanvicente/image/upload/v1628218723/master_card_nq8wvb.png' alt='master-card' width={30} height={30}/>,
	},
	{
		id: 5,
		icon: <Image src='https://res.cloudinary.com/franciscosanvicente/image/upload/v1628218723/paypal_hyobvv.png' alt='paypal-card' width={30} height={30} />,
	},

];

export const OurService = [
	{
		id: 1,
		icon: <GrDeliver />,
		title: 'same Day Delivery',
		description: 'somalia Orders Only',
	},
	{
		id: 2,
		icon: <BiSupport />,
		title: 'Best Online Support',
		description: 'Hours : 7AM - 10PM',
	},
	{
		id: 3,
		icon: <BiWorld />,
		title: 'WorldWide Free Delivery',
		description: 'For Order Over $100',
	},
	{
		id: 4,
		icon: <GrMoney />,
		title: 'Money Back Guarantee',
		description: 'Enter Now',
	},
];

export const timeStamps = [
	{
		id: 1,
		title: 'days',
		count: '714',
	},
	{
		id: 2,
		title: 'hours',
		count: '17',
	},
	{
		id: 3,
		title: 'min',
		count: '51',
	},
	{
		id: 4,
		title: 'sec',
		count: '14',
		color: true,
	},
];

// Lorem ipsum dolor sit, amet consectetur adipisicing elit. Quam, sapiente natus quibusdam culpa aliquid animi voluptatibus eaque, nisi voluptates doloremque nobis sed soluta tempora quasi aspernatur numquam rem ipsam placeat.
export const ourBlogs = [
	{

		title: 'consectetur adipisicing elit',
		text:
			'Lorem ipsum dolor sit, amet consectetur adipisicing elit. Quam, sapiente natus quibusdam culpa aliquid animi voluptatibus eaque nisi voluptates.',
		img: 1,
		date: '25 october',
		hit: 15,
	},
	{
		title: 'aliquid animi voluptatibus eaque',
		text:
			'Lorem ipsum dolor sit, amet consectetur adipisicing elit. Quam, sapiente natus quibusdam culpa aliquid animi voluptatibus eaque nisi voluptates.',
		img: 2,
		date: '16 november',
		hit: 17,
	},
	{
		title: 'sapiente natus quibusdam culpa',
		text:
			'Lorem ipsum dolor sit, amet consectetur adipisicing elit. Quam, sapiente natus quibusdam culpa aliquid animi voluptatibus eaque nisi voluptates.',
		img: 3,
		date: '26 sebtember',
		hit: 26,
	},
	{
		title: 'Lorem ipsum dolor sit',
		text:
			'Lorem ipsum dolor sit, amet consectetur adipisicing elit. Quam, sapiente natus quibusdam culpa aliquid animi voluptatibus eaque nisi voluptates.',
		img: 4,
		date: '21 december',
		hit: 22,
	},
	{
		title: 'quibusdam culpa aliquid animi',
		text:
			'Lorem ipsum dolor sit, amet consectetur adipisicing elit. Quam, sapiente natus quibusdam culpa aliquid animi voluptatibus eaque nisi voluptates.',
		img: 5,
		date: '13 julay',
		hit: 12,
	},
];

export const FooterList = [
	{
		id: 1,
		title: 'Quick Link',
		content: [
			{
				id: 1.1,
				text: 'Seller Login',
			},
			{
				text: 'Seller Sign Up',
				id: 1.2,
			},
			{
				text: 'Seller Handbook',
				id: 1.3,
			},
			{
				id: 1.4,
				text: 'Seller Control Panel',
			},
			{
				id: 1.5,
				text: 'Seller Control Panel',
			},
		],
	},
	{
		id: 2,
		title: 'Products',
		content: [
			{
				id: 2.1,
				text: 'prices drop',
			},
			{
				id: 2.2,
				text: 'new Products',
			},
			{
				id: 2.3,
				text: 'best sales',
			},
			{
				id: 2.4,
				text: 'contact us',
			},
			{
				id: 2.5,
				text: 'sitemap',
			},
		],
	},
	{
		id: 3,
		title: 'Our Company',
		content: [
			{
				id: 3.1,
				text: 'delivery',
			},
			{
				id: 3.2,
				text: 'legal notice',
			},
			{
				id: 3.3,
				text: 'terms and conditions of use',
			},
			{
				id: 3.4,
				text: 'about us',
			},
			{
				id: 3.5,
				text: 'secure payment',
			},
		],
	},

	{
		id: 4,
		title: 'your Acount',
		content: [
			{
				id: 4.1,
				text: 'personal info',
			},
			{
				id: 4.2,
				text: 'orders',
			},
			{
				id: 4.3,
				text: 'credit slips',
			},
			{
				id: 4.4,
				text: 'addresses',
			},
		],
	},

	{
		id: 5,
		title: 'store information',
		content: [
			{
				id: 5.1,
				icon: <MdLocationOn />,
				text: 'portable-laptops store USA',
			},
			{
				id: 5.2,
				icon: <MdLocalHospital />,
				text: 'portable-laptops store USA',
			},
			{
				id: 5.3,
				icon: <MdEmail />,
				text: 'portable-laptops store USA',
			},
			{
				id: 5.4,
				icon: <FaFax />,
				text: 'portable-laptops store USA',
			},
		],
	},
];
