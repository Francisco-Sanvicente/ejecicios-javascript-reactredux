import { useState, useEffect } from "react";
import clienteAxios from "../config/axios"

export const useFetch = (url) => {

    const [state, setState] = useState([]);
    const [error, setError] = useState(false);
    const [loading, setLoading] = useState(true);

    useEffect(() => {

        clienteAxios.get(url)

            .then(({ data }) => {

                setState([...data]);
                setLoading(false);

            })
            .catch((err) => {

                setError(true);

                setLoading(true);
            })

            .finally(() => {

                setLoading(false);

            });

    }, [url]);

    return {state, error, loading} ;
};
