import React from "react";
import Wobble from 'react-reveal/Wobble';
import Error404 from '../components/404/404';

const PageEror = () => {
  
  return (
    <>
      <Wobble>
        <Error404/>
      </Wobble>
    </>
  );
};

export default PageEror;
