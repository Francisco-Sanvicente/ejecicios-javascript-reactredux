import React from "react";
import Container from "@material-ui/core/Container";
import { motion } from "framer-motion";
import Featured from "../components/Cards/Featured";
const Blog = () => {
  const pageVariants = {
    initial: {
      opacity: 0,
      x: "-100vw",
      scale: 0.8,
    },
    in: {
      opacity: 1,
      x: 0,
      scale: 1,
    },
    out: {
      opacity: 0,
      x: "100vw",
      scale: 1.2,
    },
  };

  const pageTransition = {
    delay: 1,
    x: { type: "spring", stiffness: 50 },
    default: { duration: 2 },
  };

  const pageStyle = {
    position: "relative",
  };

  return (
    <>
      <motion.div
        style={pageStyle}
        initial="initial"
        animate="in"
        exit="out"
        variants={pageVariants}
        transition={pageTransition}
      >
        <Container maxWidth="lg">
          <main>
            <Featured />
          </main>
        </Container>
      </motion.div>
    </>
  );
};

export default Blog;
