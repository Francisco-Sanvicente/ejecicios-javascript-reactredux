import React from 'react';
import Fade from 'react-reveal/Fade';
import Registry from '../components/Registry/Registry'

const SignUp = () => {
  
  return (
    <>
   <Fade right big cascade>
      <Registry />
    </Fade>
    </>
  );
}
export default SignUp;