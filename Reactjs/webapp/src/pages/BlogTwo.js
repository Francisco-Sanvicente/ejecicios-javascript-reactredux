import React from "react";
import { useQuery } from "@apollo/client";
import Fade from "react-reveal/Fade";
import InfoSection from "../components/InfoSection/InfoSection";
import { homeObjOne, homeObjThree } from "../data/Data";
import { query } from "../GraphQL/query";

const BlogTwo = () => {
 
  const { data:{data} } = useQuery(query);

  const obje = {
    titulo: data[6].titulo,
    img: data[6].imagenes,
  };
  const obje2 = {
    titulo: data[8].titulo,
    img: data[8].imagenes,
  };
  return (
    <>
      <Fade bottom>
        <InfoSection {...homeObjOne} {...obje} />
      </Fade>
      <Fade right>
        <InfoSection {...homeObjThree} {...obje2} />
      </Fade>
    </>
  );
};

export default BlogTwo;
