import React  from "react";
import { motion } from "framer-motion";
import { useQuery } from "@apollo/client";
import Slide from "react-reveal/Slide";
import InfoSection from "../components/InfoSection/InfoSection";
import { homeObjOne, homeObjThree } from "../data/Data";
import { query } from "../GraphQL/query";

const BlogThree = () => {

  const { data:{data} } = useQuery(query);

  const pageVariants = {
    initial: {
      opacity: 0,
      x: "-100vw",
      scale: 0.8,
    },
    in: {
      opacity: 1,
      x: 0,
      scale: 1,
    },
    out: {
      opacity: 0,
      x: "100vw",
      scale: 1.2,
    },
  };

  const pageTransition = {
    type: "tween",
    ease: "anticipate",
    duration: 1.8,
  };

  const pageStyle = {
    position: "relative",
  };

  const obje = {
    titulo: data[5].titulo,
    img: data[5].imagenes,
  };
  const obje2 = {
    titulo: data[4].titulo,
    img: data[4].imagenes,
  };
  return (
    <>
      <motion.div
        style={pageStyle}
        initial="initial"
        animate="in"
        exit="out"
        variants={pageVariants}
        transition={pageTransition}
      >
        <InfoSection {...homeObjOne} {...obje} />

        <Slide right>
          <InfoSection {...homeObjThree} {...obje2} />
        </Slide>
      </motion.div>
    </>
  );
};

export default BlogThree;
