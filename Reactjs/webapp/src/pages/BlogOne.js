import React from "react";
import { useQuery }  from "@apollo/client";
import parse from "html-react-parser";
import Roll from "react-reveal/Roll";
import { motion } from "framer-motion";
import InfoSection from "../components/InfoSection/InfoSection";
import { homeObjOne, homeObjThree } from "../data/Data";
import { query } from "../GraphQL/query";

const BlogoNE = () => {

  const { data:{data} } = useQuery(query);

  const obje = {
    titulo: data[3].titulo,
    img: data[3].imagenes,
  };
  const obje2 = {
    titulo: parse(data[9].titulo),
    img: data[9].imagenes,
  };

  return (
    <>
      <motion.div
        animate={{ rotate: 0 }}
        transition={{ from: 90, duration: 2 }}
      >
        <InfoSection {...homeObjOne} {...obje} />
      </motion.div>
      <Roll right>
        <InfoSection {...homeObjThree} {...obje2} />
      </Roll>
    </>
  );
};

export default BlogoNE;
