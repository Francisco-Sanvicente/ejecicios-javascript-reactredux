import React, { Fragment } from "react";
import { Route, Switch, useLocation } from "react-router-dom";
import { AnimatePresence } from "framer-motion";
import Slide from "react-reveal/Slide";
import Footer from "../components/Footer/Footer";
import BlogOne from "../pages/BlogOne";
import BlogTwo from "../pages/BlogTwo";
import BlogThree from "../pages/BlogThree";
import PageError from "../pages/PageError";
import Blog from "../pages/Blog";
import Navbar from "../components/Navbar/Navbar";

const Routes = () => {
  const location = useLocation();

  return (
    <Fragment>
      <Navbar />

      <AnimatePresence>
        <Switch location={location} key={location.pathName}>
          <Route exact path="/" component={Blog} />
          <Route exact path="/blog1" component={BlogOne} />
          <Route exact path="/blog2" component={BlogTwo} />
          <Route exact path="/blog3" component={BlogThree} />
          <Route component={PageError} />
        </Switch>
      </AnimatePresence>

      <Slide right>
        <Footer />
      </Slide>
    </Fragment>
  );
};

export default Routes;
