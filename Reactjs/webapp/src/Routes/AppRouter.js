import React,{useEffect, useState} from 'react'
import { useDispatch } from 'react-redux';
import { useQuery } from "@apollo/client";
import { Switch } from 'react-router-dom';

import { PrivateRoute } from './PrivateRoute';
import { PublicRoute } from './PublicRoute';
import {firebase} from '../firebase/firebaseConfig';
import { login } from '../auth/auth';
import  Routes  from './Routes';
import Registry from '../components/Registry/Registry';
import SignInSide from '../pages/SignInSide';
import { query } from '../GraphQL/query';
import { types } from '../types/types';


export const AppRouter = () => {

    const dispatch = useDispatch();
    const { loading, error, data:{data} } = useQuery(query);

    const [ checking, setChecking ] = useState(true);
    const [ isLoggedIn, setIsLoggedIn ] = useState(false);

    if(!loading && !error && data){
        dispatch({
          type: types.db,
          payload: data,
        });
      }

    useEffect(() => {
        
        firebase.auth().onAuthStateChanged( (user) => {

            if ( user?.uid ) {
                dispatch( login( user.uid, user.displayName ) );
                setIsLoggedIn( true );
            } else {
                setIsLoggedIn( false );
            }

            setChecking(false);

        });
        
    }, [ dispatch, setChecking, setIsLoggedIn ])


    if ( checking ) {
        return (
            <h1>Espere...</h1>
        )
    }

    return (

            <>
                <Switch> 
                    <PublicRoute 
                         
                        path="/singin" 
                        component={ SignInSide } 
                        isAuthenticated={ isLoggedIn }
                    />

                        <PublicRoute 
                        exact
                        path="/singup"
                        component={ Registry }
                        isAuthenticated={ isLoggedIn }
                    />
                    
                    <PrivateRoute 
                        path="/" 
                        component={ Routes } 
                        isAuthenticated={ isLoggedIn }
                    />
                </Switch>
            </>
       
    )
}