import { types } from "../types/types";

const initialData = {
    fetching: false,
    data: [],
}

export const dataReducer = (state = initialData, action) => {
    switch (action.type) {
       
        case types.db:
            return { ...state, data: action.payload, fetching: true }

        default:
            return state
            
    }
}

