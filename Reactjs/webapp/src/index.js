import React from "react";
import ReactDOM from "react-dom";
import { Provider } from "react-redux";
import { ApolloProvider } from "@apollo/client";
import { store } from "./store/store";
import { client } from "./GraphQL/clientApllo";
import App from "./App/App";


ReactDOM.render(

  <ApolloProvider client={client}>
     <Provider store={store}>
      <App />
    </Provider>
  </ApolloProvider>,

  document.getElementById("root")
);
