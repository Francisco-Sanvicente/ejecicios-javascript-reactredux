import {gql} from "@apollo/client";

export const query = gql`
  query data {
    data  {
      titulo
      imagenes
    }
  }
`;