import { RestLink } from "apollo-link-rest";
import axios from "axios";

const urlImage = "https://comunitics.com/wp-json/wp/v2/media";

export const restLink = new RestLink({
    uri: "https://comunitics.com/wp-json/wp/v2/posts",
    responseTransformer: (response) =>
      response.json()
      .then((post) => {
        const posts = post.map((data) => ({
          titulo: data.title.rendered,
          contenido: data.content.rendered,
          extracto: data.excerpt.rendered,
          idImagen: data.featured_media,
        }));
        const imagePostPromises = posts.map((post) => {
          return axios.get(`${urlImage}/${post.idImagen}`)
            .then((imageData) => ({
              ...post,
              imagenes: imageData.data.source_url,
            }));
        });
        return axios.all(imagePostPromises);
      })
      .then((postsWithImages) => {
        console.log(postsWithImages)
        const myArray = postsWithImages;
        return myArray
      })
      .catch( error => {
        console.error("Error en la promesa ", error);
      })
  });