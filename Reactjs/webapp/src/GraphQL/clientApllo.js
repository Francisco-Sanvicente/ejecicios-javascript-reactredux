import { ApolloClient, InMemoryCache } from "@apollo/client";
//import { restLink } from "./restLink";

export const client = new ApolloClient({
  uri: 'http://localhost:4000/',
  cache: new InMemoryCache()
});