import { BrowserRouter as Router } from "react-router-dom";
import { CssBaseline } from "@material-ui/core";
import { useQuery } from "@apollo/client";
import { AppRouter } from "../Routes/AppRouter";
import Loading from "../components/Loading/Loading";
import { query } from "../GraphQL/query";
import Err from "../components/Err/Err";

const App = () => {
  
  const { loading, error } = useQuery(query);

  if (loading)
    return (
      <>
        <CssBaseline />
        <Loading />
      </>
    );

  else if (error)
    return (
      <>
        <CssBaseline />
        <Err />
      </>
    );
  return (
    <>
      <CssBaseline />
      <Router>
        <AppRouter />
      </Router>
    </>
  );
};

export default App;
