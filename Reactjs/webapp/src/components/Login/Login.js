import React from "react";
import { useDispatch, useSelector } from "react-redux";
import { Link } from "react-router-dom";
import Fade from "react-reveal/Fade";
import Lottie from "react-lottie";
import Paper from "@material-ui/core/Paper";
import Grid from "@material-ui/core/Grid";
import Typography from "@material-ui/core/Typography";
import { makeStyles } from "@material-ui/core/styles";
import blueGrey from "@material-ui/core/colors/blueGrey";
import { useTheme, useMediaQuery } from "@material-ui/core";
import Button from "@material-ui/core/Button";
import TextField from "@material-ui/core/TextField";
import FormControlLabel from "@material-ui/core/FormControlLabel";
import Checkbox from "@material-ui/core/Checkbox";

import { useForm } from "../../hooks/useForm";
import { startGoogleLogin, startLoginEmailPassword } from "../../auth/auth";

import noteData from "../../assets/user-profile.json";

const useStyles = makeStyles((theme) => ({
  root: {
    height: "100vh",
  },
  image: {
    backgroundRepeat: "no-repeat",
    backgroundColor: blueGrey[800],
    backgroundSize: "cover",
    backgroundPosition: "center",
  },
  column1: {
    margin: theme.spacing(12, 4),
    display: "flex",
    flexDirection: "column",
    alignItems: "center",
    justifyContent: "center",
  },
  paper: {
    margin: theme.spacing(8, 4),
    display: "flex",
    flexDirection: "column",
    alignItems: "center",
  },
  avatar: {
    margin: theme.spacing(1),
    backgroundColor: theme.palette.secondary.main,
  },
  form: {
    width: "100%",
    marginTop: theme.spacing(1),
  },
  column2: {
    backgroundColor: blueGrey[50],
  },
  auth__socialNetworks: {
    display: "flex",
    alignItems: "center",
    justifyContent: "center",
    flexdirection: "column",
    paddingTop: "20px",
    paddingBottom: "20px",
    width: "100%",
  },
  googleBtn: {
    cursor: "pointer",
    marginTop: "1px%",
    width: "100%",
    height: "42px",
    backgroundColor: "#4285f4",
    borderRadius: "2px",
    boxshadow: " 0 3px 4px 0 rgba(0, 0, 0, 0.25)",
    transition: ".3s ease",
  },
  googleIconWrapper: {
    position: "absolute",
    marginTop: "1px",
    marginLeft: "1px",
    width: "40px",
    height: "40px",
    borderRadius: "2px",
    backgroundColor: "#ffffff",
  },
  googleIcon: {
    position: "absolute",
    marginTop: "11px",
    marginLeft: "11px",
    width: "18px",
    height: "18px",
  },
  btnText: {
    marginLeft: "auto",
    marginRight: "auto",
    marginTop: "2%",
    color: "#ffffff",
    fontSize: "14px",
    letterSpacing: " 0.2px",
    textAlign: "center",
  },
}));
const Login = () => {
  const classes = useStyles();

  const theme = useTheme();
  const showText = useMediaQuery(theme.breakpoints.up("md"));

  const dispatch = useDispatch();
  const { loading } = useSelector((state) => state.ui);

  const initialFValues = {
    email: "emmanuelsvp@gmail.com",
    password: "123456",
    rememberPassword: false,
  };

  const { values, handleInputChange } = useForm(initialFValues, true);

  const { email, password } = values;

  const handleLogin = (e) => {
    e.preventDefault();
    // if (validate()){
    dispatch(startLoginEmailPassword(email, password));
    //  resetForm()
    // }
  };

  const handleGoogleLogin = () => {
    dispatch(startGoogleLogin());
  };

  const defaultOptions = {
    loop: true,
    autoplay: true,
    renderedSettings: {
      preseveAspectRatio: "xMdidYMid slice",
    },
  };
  return (
    <>
      <Fade top>
        <Grid container component="main" className={classes.root}>
          <Grid item xs={false} sm={4} md={7} className={classes.image}>
            {showText && (
              <div className={classes.column1}>
                <Lottie
                  options={{ animationData: noteData, ...defaultOptions }}
                  height="60%"
                  width="60%"
                />
              </div>
            )}
          </Grid>

          <Grid
            item
            xs={12}
            sm={8}
            md={5}
            component={Paper}
            elevation={6}
            square
            className={classes.column2}
          >
            <div className={classes.paper}>
              <Typography component="h1" variant="h5">
                Sign in
              </Typography>
              <Fade top big cascade>
                <form
                  className={classes.form}
                  onSubmit={handleLogin}
                  noValidate
                >
                  <TextField
                    margin="normal"
                    required
                    fullWidth
                    autoFocus
                    variant="outlined"
                    id="email"
                    type="email"
                    label="Email"
                    name="email"
                    value={email}
                    onChange={handleInputChange}
                    error={!email}
                  />
                  <TextField
                    margin="normal"
                    required
                    fullWidth
                    autoFocus
                    variant="outlined"
                    name="password"
                    label="Password"
                    type="password"
                    id="password"
                    autoComplete="current-password"
                    value={password}
                    onChange={handleInputChange}
                    error={!password}
                  />
                  <FormControlLabel
                    control={<Checkbox value="remember" color="primary" />}
                    label="Recordarme"
                  />
                  <Button
                    type="submit"
                    fullWidth
                    variant="contained"
                    color="primary"
                    className={classes.submit}
                    disable={loading}
                  >
                    Sign In
                  </Button>
                  <Grid className={classes.auth__socialNetworks}>
                    <Grid
                      className={classes.googleBtn}
                      onClick={handleGoogleLogin}
                    >
                      <Grid className={classes.googleIconWrapper}>
                        <img
                          className={classes.googleIcon}
                          src="https://upload.wikimedia.org/wikipedia/commons/5/53/Google_%22G%22_Logo.svg"
                          alt="google button"
                        />
                      </Grid>
                      <Typography className={classes.btnText}>
                        Sign in with google
                      </Typography>
                    </Grid>
                  </Grid>
                  <Grid container>
                    <Grid item xs>
                      <Link to="#" variant="body2">
                        Forgot password?
                      </Link>
                    </Grid>
                    <Grid item>
                      <Link to="/singup" variant="body2">
                        {"Don't have an account? Sign Up"}
                      </Link>
                    </Grid>
                  </Grid>
                </form>
              </Fade>
            </div>
          </Grid>
        </Grid>
      </Fade>
    </>
  );
};

export default Login;
