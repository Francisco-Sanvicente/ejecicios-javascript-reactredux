import React from "react";
import { useDispatch } from "react-redux";
import { Link } from "react-router-dom";
import Fade from "react-reveal/Fade";
import Grid from "@material-ui/core/Grid";
import Button from "@material-ui/core/Button";
import Typography from "@material-ui/core/Typography";
import { makeStyles } from "@material-ui/core/styles";
import Container from "@material-ui/core/Container";
import TextField from "@material-ui/core/TextField";
import FormControlLabel from "@material-ui/core/FormControlLabel";
import Checkbox from "@material-ui/core/Checkbox";
import { useForm } from "../../hooks/useForm";
import { startRegisterWithEmailPasswordName } from "../../auth/auth";

const useStyles = makeStyles((theme) => ({
  paper: {
    marginTop: theme.spacing(6),
    display: "flex",
    flexDirection: "column",
    alignItems: "center",
  },
  form: {
    width: "100%",
    marginTop: theme.spacing(3),
  },
  submit: {
    margin: theme.spacing(3, 0, 2),
  },
  submit1: {
    margin: theme.spacing(1, 0, 2),
  },
}));
const Registry = () => {
  const classes = useStyles();

  const dispatch = useDispatch();

  const { values, handleInputChange } = useForm(
    {
      name: "Francisco",
      email: "sanvicentepico@gmail.com",
      password: "123456",
      password2: "123456",
    },
    true
  );

  const { name, email, password, password2 } = values;

  const handleRegister = (e) => {
    e.preventDefault();

    // if (validate()) {
    dispatch(startRegisterWithEmailPasswordName(email, password, name));
    //  resetForm()
    //  }
  };

  return (
    <>
      <Fade right cascade>
        <Container component="main" maxWidth="xs">
          <div className={classes.paper}>
            <Typography component="h1" variant="h5">
              Sign up
            </Typography>
            <form className={classes.form} onSubmit={handleRegister}>
              <Grid container spacing={2}>
                <Grid item xs={12}>
                  <TextField
                    margin="normal"
                    required
                    fullWidth
                    autoFocus
                    variant="outlined"
                    autoComplete="name"
                    name="name"
                    id="firstName"
                    label="Nombre"
                    value={name}
                    onChange={handleInputChange}
                    error={!name}
                  />
                </Grid>

                <Grid item xs={12}>
                  <TextField
                    margin="normal"
                    required
                    fullWidth
                    autoFocus
                    variant="outlined"
                    id="email"
                    label="Email"
                    name="email"
                    autoComplete="email"
                    value={email}
                    onChange={handleInputChange}
                    error={!email}
                  />
                </Grid>
                <Grid item xs={12}>
                  <TextField
                    margin="normal"
                    required
                    fullWidth
                    autoFocus
                    variant="outlined"
                    name="password"
                    label="Password"
                    type="password"
                    id="password"
                    autoComplete="current-password"
                    value={password}
                    onChange={handleInputChange}
                    error={!password}
                  />
                </Grid>

                <Grid item xs={12}>
                  <TextField
                    margin="normal"
                    required
                    fullWidth
                    autoFocus
                    variant="outlined"
                    name="password2"
                    label="Repetir Password"
                    type="password"
                    id="password2"
                    autoComplete="current-password"
                    value={password2}
                    onChange={handleInputChange}
                    error={!password2}
                  />
                </Grid>
                <Grid item xs={12}>
                  <FormControlLabel
                    control={
                      <Checkbox value="allowExtraEmails" color="primary" />
                    }
                    label="Quiero recibir promociones de marketing y actualizaciones por correo electrónico."
                  />
                </Grid>
              </Grid>
              <Button
                type="submit"
                fullWidth
                variant="contained"
                color="primary"
                className={classes.submit}
              >
                Sign Up
              </Button>
              <Grid container justify="flex-end">
                <Grid item>
                  <Link to="/singin" variant="body2">
                    Ya esta registrado? Sign in
                  </Link>
                </Grid>
              </Grid>
            </form>
          </div>
        </Container>
      </Fade>
    </>
  );
};

export default Registry;
