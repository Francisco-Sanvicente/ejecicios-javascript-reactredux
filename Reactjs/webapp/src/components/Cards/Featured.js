import React from "react";
import { useSelector } from "react-redux";
import Grid from "@material-ui/core/Grid";
import parse from "html-react-parser";
import Roll from "react-reveal/Roll";
import Bounce from "react-reveal/Bounce";
import Zoom from "react-reveal/Zoom";
import MainFeatured from "./MainFeatured";
import CardPreview from "./CardPreview";

const Featured = () => {

  const { data } = useSelector((state) => state.data);
  return (
    <>
      <Grid container spacing={3}>
        <Grid item xs={12}>
          {/* <Roll left> */}
          <MainFeatured />
          {/* </Roll> */}
        </Grid>

        <Grid item xs={12} sm={6}>
          <Bounce left>
            <CardPreview
              url={parse(data[1].imagenes)}
              titulo={parse(data[1].titulo)}
            />
          </Bounce>
        </Grid>

        <Grid item xs={12} sm={6}>
          <Roll right>
            <CardPreview
              url={parse(data[7].imagenes)}
              titulo={parse(data[7].titulo)}
            />
          </Roll>
        </Grid>

        <Grid item xs={6} sm={4} md={3}>
          <Zoom>
            <CardPreview
              url={parse(data[2].imagenes)}
              titulo={parse(data[2].titulo)}
            />
          </Zoom>
        </Grid>
        <Grid item xs={6} sm={4} md={3}>
          <Zoom>
            <CardPreview
              url={parse(data[4].imagenes)}
              titulo={parse(data[4].titulo)}
            />
          </Zoom>
        </Grid>
        <Grid item xs={6} sm={4} md={3}>
          <Zoom>
            <CardPreview
              url={parse(data[5].imagenes)}
              titulo={parse(data[5].titulo)}
            />
          </Zoom>
        </Grid>
        <Grid item xs={6} sm={4} md={3}>
          <Zoom>
            <CardPreview
              url={parse(data[6].imagenes)}
              titulo={parse(data[6].titulo)}
            />
          </Zoom>
        </Grid>
      </Grid>
    </>
  );
};
export default Featured;
