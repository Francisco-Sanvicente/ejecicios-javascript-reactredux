import React from "react";
import { useSelector } from "react-redux";
import parse from "html-react-parser";
import { makeStyles } from "@material-ui/core/styles";
import Typography from "@material-ui/core/Typography";
import Grid from "@material-ui/core/Grid";

const useStyles = makeStyles((theme) => ({
  mainFeatured: {
    height: 500,
    color: theme.palette.common.black,
    marginBottom: theme.spacing(4),
    backgroundSize: "cover",
    backgroundRepeat: "no-repeat",
    backgroundPosition: "center",
    marginTop: "20px",
  },
  mainFeaturedContent: {
    position: "relative",
    padding: theme.spacing(3),
    [theme.breakpoints.up("md")]: {
      padding: theme.spacing(6),
      paddingRight: 0,
    },
  },
}));
const MainFeatured = () => {

  const classes = useStyles();
  const { data } = useSelector((state) => state.data);

  const imagen = data[0].imagenes

  return (
    <>
    <Grid container className={classes.mainFeatured} style={{backgroundImage:`url(${imagen})`}} >
      <Grid item md={6} className={classes.mainFeaturedContent} >
      <Typography component="h1" variant="h3" color="inherit" gutterBottom>
        {parse(data[0].titulo)}
        </Typography>
      </Grid>
    </Grid>
    </>
  );
}
export default MainFeatured;