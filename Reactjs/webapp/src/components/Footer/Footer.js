import React from "react";
import { makeStyles } from "@material-ui/core/styles";
import Container from "@material-ui/core/Container";
import Typography from "@material-ui/core/Typography";
const useStyles = makeStyles((theme) => ({
  footer: {
    padding: theme.spacing(8),
  },
}));

const Footer = () => {
  const classes = useStyles();
  return (
    <>
      <footer className={classes.footer}>
        <Container maxWidth="lg">
          <Typography variant="h6" align="center" gutterBottom>
            Francisco Sanvicente
          </Typography>
          <Typography variant="body2" color="textSecondary" align="center">
            {"Copyright © "}
            {" " + new Date().getFullYear()}
          </Typography>
        </Container>
      </footer>
    </>
  );
}
export default Footer;