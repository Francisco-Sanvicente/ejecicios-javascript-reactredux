import React from "react";
import { Link } from "react-router-dom";
import Tada from "react-reveal/Tada";
import Pulse from "react-reveal/Pulse";
import PropTypes from "prop-types";
import { Container, Button } from "../../style/globalStyles";

import {
  InfoSec,
  InfoRow,
  InfoColumn,
  TextWrapper,
  TopLine,
  Heading,
  Subtitle,
  ImgWrapper,
  Img,
} from "../../style/InfoSection.elements";

const InfoSection = ({
  primary,
  lightBg,
  topLine,
  lightTopLine,
  lightText,
  lightTextDesc,
  titulo,
  description,
  buttonLabel,
  img,
  alt,
  imgStart,
  start,
}) => {
  return (
    <>
      <InfoSec lightBg={lightBg}>
        <Container>
          <InfoRow imgStart={imgStart}>
            <InfoColumn>
              <TextWrapper>
                <TopLine lightTopLine={lightTopLine}>{topLine}</TopLine>
                <Heading lightText={lightText}>{titulo}</Heading>
                <Subtitle lightTextDesc={lightTextDesc}>{description}</Subtitle>
                <Tada>
                  <Link to="/">
                    <Button big fontBig primary={primary}>
                      {buttonLabel}
                    </Button>
                  </Link>
                </Tada>
              </TextWrapper>
            </InfoColumn>
            <InfoColumn>
              <Pulse>
                <ImgWrapper start={start}>
                  <Img src={img} alt={alt} />
                </ImgWrapper>
              </Pulse>
            </InfoColumn>
          </InfoRow>
        </Container>
      </InfoSec>
    </>
  );
};

export default InfoSection;

InfoSection.propTypes = {
  lightBg: PropTypes.bool.isRequired,
  lightText: PropTypes.bool.isRequired,
  lightTextDesc: PropTypes.bool.isRequired,
  topLine: PropTypes.string.isRequired,
  description: PropTypes.string.isRequired,
  buttonLabel: PropTypes.string.isRequired,
  imgStart: PropTypes.string.isRequired,
  alt: PropTypes.string.isRequired,
  img: PropTypes.any.isRequired,
  titulo: PropTypes.string.isRequired,
};
