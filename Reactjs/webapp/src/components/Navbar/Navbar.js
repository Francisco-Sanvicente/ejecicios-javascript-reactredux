import React from "react";
import { useDispatch } from 'react-redux'
import { makeStyles, Toolbar, Typography, Button } from "@material-ui/core";
import MenuIcon from "@material-ui/icons/Menu";
import Fade from "react-reveal/Fade";
import Sidebar from "./Sidebar";
import { startLogout } from '../../auth/auth';
const useStyles = makeStyles(() => ({
  menu: {
    color: "white",
  },
  title: {
    flexGrow: 1,
  },
  root: {
    background: "linear-gradient(45deg,#1F1C18 30%, #6f0000 90%)",
    border: 0,
    boxShadow: "0 3px 5px 2px #0000004c",
    color: "white",
  },
  boton: {
    background:
      "linear-gradient(45deg, rgb(1, 11, 70)  30%, rgb(3, 223, 238) 90%)",
    borderRadius: "3px",
    fontSize: "16px",
    border: 0,
    color: "white",
    height: "48px",
    padding: " 0 30px",
    boxShadow: " 0 3px 5px 2px rgba(255, 105, 135, 0.3)",
  },
}));
const Navbar = () => {
  const classes = useStyles();
  const dispatch = useDispatch()
  
  const handleLogout = () => {
    dispatch( startLogout() )
  };
  return (
    <>
      <>
        <Fade>
          <Toolbar className={classes.root} color="primary">
            <Sidebar>
              {/* <IconButton className={classes.menu}> */}
              <MenuIcon className={classes.menu} />
              {/* </IconButton> */}
            </Sidebar>
            <Typography variant="h6" className={classes.title}>
              Blogging Website
            </Typography>
            <Button
              color="primary"
              className={classes.boton}
              onClick={handleLogout}
            >
              Logout
            </Button>
           
          </Toolbar>
        </Fade>
      </>
    </>
  );
};

export default Navbar;
