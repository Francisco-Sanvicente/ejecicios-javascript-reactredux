import React from "react";
import Lottie from "react-lottie";
import noteData from "../../assets/404-robot-error.json";

const Error404 = () => {

  const handleError = ({ history }) => {
    // history.push('/');
    history.replace("/");
  };

  const defaultOptions = {
    loop: true,
    autoplay: true,
    renderedSettings: {
      preseveAspectRatio: "xMdidYMid slice",
    },
  };
  return (
    <div className="container mx-auto px-4">
      <section className="py-8 px-4 text-center">
        <div className="max-w-auto mx-auto">
          <div className="md:max-w-lg mx-auto">
            <Lottie
              options={{ animationData: noteData, ...defaultOptions }}
              width="60%"
              height="60%"
            />
          </div>
          <h2 className="mt-8 text-xl lg:text-4xl font-black">
            Lo sentimos, ¡página no encontrada!
          </h2>
          <p className="mt-6 uppercase text-sm lg:text-base text-gray-900">
            Es posible que la página que está buscando se haya eliminado, se
            cambió el nombre o no está disponible temporalmente.
          </p>
          <button
            onClick={handleError}
            className="mt-6 bg-blue-700 hover:bg-blue-900 text-white font-light py-4 px-6 rounded-full inline-block uppercase shadow-md"
          >
            Volver a la página de inicio
          </button>
        </div>
      </section>
    </div>
  );
};

export default Error404;
