const cargarAPIBtn = document.querySelector("#cargarRESTAPI");
cargarAPIBtn.addEventListener("click", obtenerDatos); //Esperando al evento click en el boton
const urlRestApi = "https://comunitics.com/wp-json/wp/v2/posts";
const urlImage = "https://comunitics.com/wp-json/wp/v2/media";
function obtenerDatos() {
  try {
  //   fetch(urlRestApi) //Llamada a las rest api
  // .then(respuesta => respuesta.json()) //convertir la respuesta a json
  // .then(data => {
  //   const posts = data.map(data => ({ //recorrer la data
  //     titulo: data.title.rendered,
  //     contenido: data.content.rendered,  //asignar y obetener lo que quiero
  //     extracto: data.excerpt.rendered,
  //     idImagen: data.featured_media,
  //   }));
  //   const imagePostPromises = posts.map(post => { //recorrer el array anterior
  //     return fetch(`${urlImage}/${post.idImagen}`) //pasar cada id a la url de las imagenes de wordpress
  //       .then(res => res.json())   //converti la respuesta a json
  //       .then(imageData => ({ ...post,    
  //           imagenes: imageData.source_url, //asignar lo que quiero el array anterior las url de imagen
  //       }));
  //   });
  //   return Promise.all(imagePostPromises); // promises.all garantisa que de una respuesta despues que todas las promesas 
  // })                                      // se hayan ejecutado si no habra 10 respuesta en el proceso anterior por el
  // .then(postsWithImages => {              // recorrido del array
  //   console.log(postsWithImages);        //la respueta del promises.all tengo todo 
  //   mostrarHTML(postsWithImages)        //llamo a la funcion que imprime, paso el array con todos los datos
  // });
  axios.get(`${urlRestApi}`).then(post=>{ 
    const posts = post.data.map(data => ({ //recorrer la data
          titulo: data.title.rendered,
          contenido: data.content.rendered,  //asignar y obetener lo que quiero
          extracto: data.excerpt.rendered,
          idImagen: data.featured_media,
    })); 
    const imagePostPromises = posts.map(post => {
      return axios.get( `${urlImage}/${post.idImagen}`) //pasar cada id a la url de las imagenes de wordpress
      .then(imageData => ({...post,
         imagenes: imageData.data.source_url, //asignar lo que quiero el array anterior las url de imagen
      }));
    })
   return axios.all(imagePostPromises);
  }).then(postsWithImages =>{ // recorrido del array
    console.log(postsWithImages); //la respueta del promises.all tengo todo 
    mostrarHTML(postsWithImages) //llamo a la funcion que imprime, paso el array con todos los datos
  });
  } catch (e) {
    console.log("error");
    console.log(e);
  }
}
function mostrarHTML(postsWithImages) {
  const contenido = document.querySelector("#contenido");
  contenido.innerHTML = `
          <a href="${postsWithImages[0].imagenes}" target="_blank">Ver Imagen</a>
          <img src="${postsWithImages[0].imagenes}"  alt="Imagen"/>
          <h1>  ${postsWithImages[0].titulo}</h1>
          <div> ${postsWithImages[0].contenido}</div>
          <div> ${postsWithImages[0].extracto}</div>
      `;  
}
