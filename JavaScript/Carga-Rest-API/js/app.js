const cargarAPIBtn = document.querySelector("#cargarRESTAPI");
cargarAPIBtn.addEventListener("click", obtenerDatos); //Esperando al evento click en el boton
 const urlRestApi = "https://comunitics.com/wp-json/wp/v2/posts";
const urlImage = "https://comunitics.com/wp-json/wp/v2/media"; 
 const datos = obtenerDatos;
 function obtenerDatos ()  {

  try {
      fetch(urlRestApi) //Llamada a las rest api
    .then(respuesta => respuesta.json()) //convertir la respuesta a json
    .then(data => {
      const posts = data.map(data => ({ //recorrer la data
        titulo: data.title.rendered,
        contenido: data.content.rendered,  //asignar y obetener lo que quiero
        extracto: data.excerpt.rendered,
        idImagen: data.featured_media,
      }));
      const imagePostPromises = posts.map(post => { //recorrer el array anterior
        return fetch(`${urlImage}/${post.idImagen}`) //pasar cada id a la url de las imagenes de wordpress
          .then(res => res.json())   //converti la respuesta a json
          .then(imageData => ({ ...post,
              imagenes: imageData.source_url, //asignar lo que quiero el array anterior las url de imagen
          }));
      });
      return Promise.all(imagePostPromises); // promises.all garantisa que de una respuesta despues que todas las promesas
    })                                      // se hayan ejecutado si no habra 10 respuesta en el proceso anterior por el
    .then(postsWithImages => {              // recorrido del array
      console.log(postsWithImages);        //la respueta del promises.all tengo todo
      mostrarHTML(postsWithImages)        //llamo a la funcion que imprime, paso el array con todos los datos
    });
    // axios
    //   .get(`${urlRestApi}`)
    //   .then((post) => {
    //     const posts = post.data.map((data) => ({
    //       //recorrer la data
    //       titulo: data.title.rendered,
    //       contenido: data.content.rendered, //asignar y obetener lo que quiero
    //       extracto: data.excerpt.rendered,
    //       idImagen: data.featured_media,
    //     }));
    //     const imagePostPromises = posts.map((post) => {
    //       return axios
    //         .get(`${urlImage}/${post.idImagen}`) //pasar cada id a la url de las imagenes de wordpress
    //         .then((imageData) => ({
    //           ...post,
    //           imagenes: imageData.data.source_url, //asignar lo que quiero el array anterior las url de imagen
    //         }));
    //     });
    //     return axios.all(imagePostPromises);
    //   })
    //   .then((postsWithImages) => {
    //     // recorrido del array
    //     console.log(postsWithImages); //la respueta del promises.all tengo todo
    //     mostrarHTML(postsWithImages); //llamo a la funcion que imprime, paso el array con todos los dato
       
    //   });
  } catch (e) {
    console.log("error");
    console.log(e);
  }
}

function mostrarHTML(postsWithImages) {
  const contenido = document.querySelector("#imagen0 ");
  contenido.innerHTML = `<img class="md:h-1/2 w-full shadow" src="${postsWithImages[0].imagenes}"  alt="Imagen"/>`;
  const contenido1 = document.querySelector("#titulo1");
  contenido1.innerHTML = `<h1>  ${postsWithImages[0].titulo}</h1>`;
  const contenido2 = document.querySelector("#parrafo1");
  contenido2.innerHTML = ` <p class="text-gray-800 font-serif text-base px-6 mb-5">${postsWithImages[0].extracto}</p> `;
  const contenido4 = document.querySelector("#titulo2");
  contenido4.innerHTML = ` <h1>  ${postsWithImages[1].titulo}</h1>`;
  const contenido3 = document.querySelector("#imagen1");
  contenido3.innerHTML = `<img class="h-64 w-full rounded-t pb-6"  src="${postsWithImages[1].imagenes}"  alt="Imagen"/>`;
  const contenido6 = document.querySelector("#titulo3");
  contenido6.innerHTML = ` <h1>  ${postsWithImages[2].titulo}</h1>`;
  const contenido5 = document.querySelector("#imagen2");
  contenido5.innerHTML = `<img class="h-64 w-full rounded-t pb-6"  src="${postsWithImages[2].imagenes}"  alt="Imagen"/>`;
  const contenido7 = document.querySelector("#titulo4");
  contenido7.innerHTML = ` <h1>  ${postsWithImages[3].titulo}</h1>`;
  const contenido8 = document.querySelector("#imagen3");
  contenido8.innerHTML = `<img class="h-64 w-full rounded-t pb-6"  src="${postsWithImages[3].imagenes}"  alt="Imagen"/>`;
  const contenido9 = document.querySelector("#titulo5");
  contenido9.innerHTML = ` <h1>  ${postsWithImages[4].titulo}</h1>`;
  const contenido10 = document.querySelector("#imagen4");
  contenido10.innerHTML = `<img class="h-full w-full rounded-t pb-6"  src="${postsWithImages[4].imagenes}"  alt="Imagen"/>`;
  const contenido11 = document.querySelector("#titulo6");
  contenido11.innerHTML = ` <h1>  ${postsWithImages[5].titulo}</h1>`;
  const contenido12 = document.querySelector("#imagen5");
  contenido12.innerHTML = `<img class="h-full w-full rounded-t pb-6"  src="${postsWithImages[5].imagenes}"  alt="Imagen"/>`;
  const contenido13 = document.querySelector("#titulo7");
  contenido13.innerHTML = ` <h1>  ${postsWithImages[6].titulo}</h1>`;
  const contenido14 = document.querySelector("#imagen6");
  contenido14.innerHTML = `<img class="h-full w-full rounded-t pb-6"  src="${postsWithImages[6].imagenes}"  alt="Imagen"/>`;
  const contenido15 = document.querySelector("#titulo8");
  contenido15.innerHTML = ` <h1>  ${postsWithImages[7].titulo}</h1>`;
  const contenido16 = document.querySelector("#imagen7");
  contenido16.innerHTML = `<img class="h-full w-full rounded-t pb-6"  src="${postsWithImages[7].imagenes}"  alt="Imagen"/>`;
}
